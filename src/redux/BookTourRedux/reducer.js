import Immutable from "seamless-immutable";
import { UpdateTourTypes } from "./actions";

import { makeReducerCreator } from "../../utils/reduxUtils";

export const INITIAL_STATE = Immutable({
  data: null,
  tourId: null,
  error: null,
  isCartAvailable: true,
  promotion: null,
  updateTourLoading: false,
  addSiteToTourLoading: false,
  getTourLoading: false,
  checkPromotionLoading: false,
  createTourLoading: false,
  deleteTourLoading: false,
});

const createTour = (state) =>
  state.merge({
    createTourLoading: true,
  });

const createTourSuccess = (state, { response }) =>
  state.merge({
    data: response.data,
    tourId: response.data.id,
    createTourLoading: false,
    isCartAvailable: true,
  });

const createTourFailure = (state, { error }) =>
  state.merge({
    error: error,
    createTourLoading: false,
    isCartAvailable: false,
  });

const deleteTour = (state) =>
  state.merge({
    deleteTourLoading: true,
  });

const deleteTourSuccess = (state, { response }) =>
  state.merge({
    tourId: null,
    deleteTourLoading: false,
    isCartAvailable: true,
  });

const deleteTourFailure = (state, { error }) =>
  state.merge({
    error: error,
    deleteTourLoading: false,
  });

const updateTour = (state) =>
  state.merge({
    updateTourLoading: true,
  });

const updateTourSuccess = (state, { response }) =>
  state.merge({
    data: response.data.status === 3 ? null : response.data,
    isCartAvailable: response.data.status === 3 ? true : false,
    updateTourLoading: false,
    tourId: response.data.status === 3 ? null : response.data.tourId,
  });

const updateTourFailure = (state, { error }) =>
  state.merge({
    error: error,
    updateTourLoading: false,
  });

const addSiteToTour = (state) =>
  state.merge({
    addSiteToTourLoading: true,
  });

const addSiteToTourSuccess = (state, { response }) =>
  state.merge({
    addSiteToTourLoading: false,
  });

const addSiteToTourFailure = (state, { error }) =>
  state.merge({
    error: error,
    addSiteToTourLoading: false,
  });

const getTour = (state, { error }) =>
  state.merge({
    getTourLoading: true,
  });

const getTourSuccess = (state, { response }) =>
  // console.log("tokem", response.data.id);

  state.merge({
    getTourLoading: false,
    data: response.data.status === 3 ? null : response.data,
    tourId: response.data.status === 3 ? null : response.data.id,
  });

const getTourFailure = (state, { error }) =>
  state.merge({
    error: error,
    getTourLoading: false,
  });

const checkPromotion = (state, { error }) =>
  state.merge({
    checkPromotionLoading: true,
  });

const checkPromotionSuccess = (state, { response }) =>
  state.merge({
    checkPromotionLoading: false,
    promotion: response.data,
  });

const checkPromotionFailure = (state, { error }) =>
  state.merge({
    error: error,
    checkPromotionLoading: false,
  });

export const deleteTourId = (state, { location }) =>
  state.merge({
    tourId: null,
    deleteTourLoading: false,
    isCartAvailable: true,
  });

const ACTION_HANDLERS = {
  [UpdateTourTypes.CREATE_TOUR]: createTour,
  [UpdateTourTypes.CREATE_TOUR_SUCCESS]: createTourSuccess,
  [UpdateTourTypes.CREATE_TOUR_FAILURE]: createTourFailure,

  [UpdateTourTypes.UPDATE_TOUR]: updateTour,
  [UpdateTourTypes.UPDATE_TOUR_SUCCESS]: updateTourSuccess,
  [UpdateTourTypes.UPDATE_TOUR_FAILURE]: updateTourFailure,

  [UpdateTourTypes.ADD_SITE_TO_TOUR]: addSiteToTour,
  [UpdateTourTypes.ADD_SITE_TO_TOUR_SUCCESS]: addSiteToTourSuccess,
  [UpdateTourTypes.ADD_SITE_TO_TOUR_FAILURE]: addSiteToTourFailure,

  [UpdateTourTypes.GET_TOUR]: getTour,
  [UpdateTourTypes.GET_TOUR_SUCCESS]: getTourSuccess,
  [UpdateTourTypes.GET_TOUR_FAILURE]: getTourFailure,

  [UpdateTourTypes.CHECK_PROMOTION]: checkPromotion,
  [UpdateTourTypes.CHECK_PROMOTION_SUCCESS]: checkPromotionSuccess,
  [UpdateTourTypes.CHECK_PROMOTION_FAILURE]: checkPromotionFailure,

  [UpdateTourTypes.DELETE_TOUR]: deleteTourId,
  [UpdateTourTypes.DELETE_TOUR_SUCCESS]: deleteTourSuccess,
  [UpdateTourTypes.DELETE_TOUR_FAILURE]: deleteTourFailure,
};

export default makeReducerCreator(INITIAL_STATE, ACTION_HANDLERS);
