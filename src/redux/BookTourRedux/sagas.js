import { put, call, takeLatest } from "redux-saga/effects";
import TourActions, { UpdateTourTypes } from "./actions";
import {
  updateTourApi,
  addSiteToTourApi,
  getTourApi,
  checkPromotionApi,
  createTourApi,
} from "../../api/tour";
import { Alert } from "react-native";
import { NavigationUtils } from "../../navigation";

export function* createTourSaga({ data }) {
  try {
    const response = yield call(createTourApi, data);
    yield put(TourActions.createTourSuccess(response));
    yield put(TourActions.getTour(response.data.id));
  } catch (error) {
    yield put(TourActions.createTourFail(error));
    // if (error.code === 403 || error.code === 409) {
    //   return showInAppNotification("Sign In", error.message, "error");
    // }
    // return showInAppNotification("Sign In", "Check your connection", "error");
  }
}

export function* updateTourSaga({ data }) {
  try {
    const response = yield call(updateTourApi, data);
    console.log(response);
    yield put(TourActions.updateTourSuccess(response));
    yield put(TourActions.getTour(response.data.id));
    yield NavigationUtils.pop();
  } catch (error) {
    yield put(TourActions.updateTourFail(error));
    // NavigationUtils.pop();
    // if (error.code === 403 || error.code === 409) {
    //   return showInAppNotification("Sign In", error.message, "error");
    // }
    // return showInAppNotification("Sign In", "Check your connection", "error");
  }
}

export function* addSiteToTourSaga({ data }) {
  try {
    const response = yield call(addSiteToTourApi, data);
    Alert.alert("Success", "Add site to tour successfully");
    console.log(response);
  } catch (error) {
    console.log(error);

    if (error.code === 404) {
      Alert.alert("Warning", "Site already add to tour");

      // return showInAppNotification("Sign In", error.message, "error");
      // Alert.alert(error.message);
    }
    // return showInAppNotification("Sign In", "Check your connection", "error");
  }
}

export function* getTourSaga({ data }) {
  try {
    const response = yield call(getTourApi, data);
    // Alert.alert("Success", "Add site to tour successfully");
    yield put(TourActions.getTourSuccess(response));
    console.log(response);
  } catch (error) {
    console.log(error);
    if (error.code === 404) {
      Alert.alert("Error", "Some thing wrong");
      yield put(TourActions.getTourFail(error));
      // return showInAppNotification("Sign In", error.message, "error");
      // Alert.alert(error.message);
    }
    // return showInAppNotification("Sign In", "Check your connection", "error");
  }
}

export function* checkPromotionSaga({ data }) {
  try {
    const response = yield call(checkPromotionApi, data);
    // Alert.alert("Success", "Add site to tour successfully");
    yield put(TourActions.checkPromotionSuccess(response));
    console.log(response);
    Alert.alert("D Travel", "D Travel have applied the promotion to your tour");
  } catch (error) {
    console.log(error);
    // if (error.code === 404) {
    Alert.alert("Error", error.message);
    // yield put(TourActions.checkPromotionFail(error));
    // return showInAppNotification("Sign In", error.message, "error");
    // Alert.alert(error.message);
    // }
    // return showInAppNotification("Sign In", "Check your connection", "error");
  }
}

const tourSagas = () => [
  takeLatest(UpdateTourTypes.UPDATE_TOUR, updateTourSaga),
  takeLatest(UpdateTourTypes.ADD_SITE_TO_TOUR, addSiteToTourSaga),
  takeLatest(UpdateTourTypes.GET_TOUR, getTourSaga),
  takeLatest(UpdateTourTypes.CHECK_PROMOTION, checkPromotionSaga),
  takeLatest(UpdateTourTypes.CREATE_TOUR, createTourSaga),
];

export default tourSagas();
