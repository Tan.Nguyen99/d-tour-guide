import { makeActionCreator, makeConstantCreator } from "../../utils/reduxUtils";

export const UpdateTourTypes = makeConstantCreator(
  "CREATE_TOUR",
  "CREATE_TOUR_SUCCESS",
  "CREATE_TOUR_FAIL",

  "UPDATE_TOUR",
  "UPDATE_TOUR_SUCCESS",
  "UPDATE_TOUR_FAIL",

  "ADD_SITE_TO_TOUR",
  "ADD_SITE_TO_TOUR_SUCCESS",
  "ADD_SITE_TO_TOUR_FAIL",

  "GET_TOUR",
  "GET_TOUR_SUCCESS",
  "GET_TOUR_FAIL",

  "CHECK_PROMOTION",
  "CHECK_PROMOTION_SUCCESS",
  "CHECK_PROMOTION_FAIL",

  "DELETE_TOUR",
  "DELETE_TOUR_SUCCESS",
  "DELETE_TOUR_FAIL"
);

const updateTour = (data) =>
  makeActionCreator(UpdateTourTypes.UPDATE_TOUR, { data });
const updateTourSuccess = (response) =>
  makeActionCreator(UpdateTourTypes.UPDATE_TOUR_SUCCESS, { response });
const updateTourFail = (error) =>
  makeActionCreator(UpdateTourTypes.UPDATE_TOUR_FAIL, { error });

const addSiteToTour = (data) =>
  makeActionCreator(UpdateTourTypes.ADD_SITE_TO_TOUR, { data });
const addSiteToTourSuccess = (response) =>
  makeActionCreator(UpdateTourTypes.ADD_SITE_TO_TOUR_SUCCESS, { response });
const addSiteToTourFail = (error) =>
  makeActionCreator(UpdateTourTypes.ADD_SITE_TO_TOUR_FAIL, { error });

const getTour = (data) => makeActionCreator(UpdateTourTypes.GET_TOUR, { data });
const getTourSuccess = (response) =>
  makeActionCreator(UpdateTourTypes.GET_TOUR_SUCCESS, { response });
const getTourFail = (error) =>
  makeActionCreator(UpdateTourTypes.GET_TOUR_FAIL, { error });

const checkPromotion = (data) =>
  makeActionCreator(UpdateTourTypes.CHECK_PROMOTION, { data });
const checkPromotionSuccess = (response) =>
  makeActionCreator(UpdateTourTypes.CHECK_PROMOTION_SUCCESS, { response });
const checkPromotionFail = (error) =>
  makeActionCreator(UpdateTourTypes.CHECK_PROMOTION_FAIL, { error });

const createTour = (data) =>
  makeActionCreator(UpdateTourTypes.CREATE_TOUR, { data });
const createTourSuccess = (response) =>
  makeActionCreator(UpdateTourTypes.CREATE_TOUR_SUCCESS, { response });
const createTourFail = (error) =>
  makeActionCreator(UpdateTourTypes.CREATE_TOUR_FAIL, { error });

const deleteTour = (data) =>
  makeActionCreator(UpdateTourTypes.DELETE_TOUR, { data });
const deleteTourSuccess = (response) =>
  makeActionCreator(UpdateTourTypes.DELETE_TOUR_SUCCESS, { response });
const deleteTourFail = (error) =>
  makeActionCreator(UpdateTourTypes.DELETE_TOUR_FAIL, { error });

export default {
  addSiteToTour,
  addSiteToTourSuccess,
  addSiteToTourFail,

  updateTour,
  updateTourSuccess,
  updateTourFail,

  getTour,
  getTourSuccess,
  getTourFail,

  checkPromotion,
  checkPromotionSuccess,
  checkPromotionFail,

  createTour,
  createTourSuccess,
  createTourFail,

  deleteTour,
  deleteTourSuccess,
  deleteTourFail,
};
