import login from "./AuthRedux/reducer";
import app from "./AppRedux/reducer";
import check from "./ChekingRedux/reducer";
import site from "./SiteRedux/reducer";
import tour from "./BookTourRedux/reducer";

export default {
  login,
  app,
  check,
  site,
  tour,
};
