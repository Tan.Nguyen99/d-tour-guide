import Immutable from "seamless-immutable";
import { AppTypes } from "./actions";
import { makeReducerCreator } from "../../utils/reduxUtils";

export const INITIAL_STATE = Immutable({
  networkStatus: null,
  currentLocation: null,
  tourStatus: 1,
  data: null,
});

export const changeNetworkStatus = (state, { status }) =>
  state.merge({ networkStatus: status });
export const getCurrentLocation = (state, { location }) =>
  state.merge({ currentLocation: location });
export const getTourStatus = (state, { data }) =>
  state.merge({ tourStatus: data });

export const setBookTourData = (state, { data }) => state.merge({ data: data });

const ACTION_HANDLERS = {
  [AppTypes.CHANGE_NETWORK_STATUS]: changeNetworkStatus,
  [AppTypes.GET_CURRENT_LOCATION]: getCurrentLocation,
  [AppTypes.TOUR_STATUS]: getTourStatus,
  [AppTypes.BOOK_TOUR_DATA]: setBookTourData,
};

export default makeReducerCreator(INITIAL_STATE, ACTION_HANDLERS);
