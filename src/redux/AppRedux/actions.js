import { makeActionCreator, makeConstantCreator } from "../../utils/reduxUtils";

export const AppTypes = makeConstantCreator(
  "STARTUP",
  "CHANGE_NETWORK_STATUS",
  "GET_CURRENT_LOCATION",
  "TOUR_STATUS",
  "BOOK_TOUR_DATA"
);

const startup = () => makeActionCreator(AppTypes.STARTUP);

const changeNetworkStatus = (status) =>
  makeActionCreator(AppTypes.CHANGE_NETWORK_STATUS, { status });

const getCurrentLocation = (location) =>
  makeActionCreator(AppTypes.GET_CURRENT_LOCATION, { location });

const getTourStatus = (data) =>
  makeActionCreator(AppTypes.TOUR_STATUS, { data });

const setBookTourData = (data) =>
  makeActionCreator(AppTypes.BOOK_TOUR_DATA, { data });

export default {
  startup,
  changeNetworkStatus,
  getCurrentLocation,
  getTourStatus,
  setBookTourData,
};
