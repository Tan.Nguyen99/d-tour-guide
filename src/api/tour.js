import { get, post, put, del } from "./utils";

export async function createTourApi(data) {
  return post("/tour", data);
}
export async function addSiteToTourApi(data) {
  return post("/detail", data);
}

export async function deleteSiteInTourApi(data) {
  return del(`/detail/${data}`);
}

export async function updateTourApi(data) {
  return put(`/tour/${data.id}`, data.data);
}
// export async function getTourApi(data) {
//   return get("/tour-by-tourist-with-status", data);
// }
export async function getTourApi(data) {
  return get(`/tour/${data}`);
}

export async function checkPromotionApi(data) {
  return post("/check-promotion", data);
}

export async function getTourByStatusApi(data) {
  return get("/tour-by-status", data);
}

export async function getHistoryApi(data) {
  return get("/history", data);
}
