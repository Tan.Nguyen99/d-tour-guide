import { get, post, put, del } from "./utils";

export async function getAllSiteApi() {
  return get("/site-category");
}

export async function getSiteByIdApi(id) {
  return get(`/tourist-site/${id}`);
}

export async function getAllPlaceApi() {
  return get("/tourist-site");
}

export async function ratingPLaceApi(data) {
  return post("/rating", data);
}

export async function searchPLaceApi(data) {
  return get("/search", data);
}

//

// export async function sentDeviceTokenByIdApi(data) {
//   return put(`/${data.userId}/${data.deviceToken}`);
// }
