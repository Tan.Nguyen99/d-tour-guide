import { get, post, put, del } from "./utils";

export async function adminLoginApi(data) {
  return post("/login", data);
}

export async function registerApi(data) {
  return post("/register", data);
}

export async function getUserApi() {
  return get("/user");
}

export async function getUserDetailApi(id) {
  return get(`/tourist/${id}`);
}

export async function getVerifyCodeApi(data) {
  return get("/verify-code", data);
}

export async function verifyEmailApi(data) {
  return post("/verify-account", data);
}

export async function getNotificationApi(data) {
  return get("/notification", data);
}
// export async function sentDeviceTokenByIdApi(data) {
//   return put(`/${data.userId}/${data.deviceToken}`);
// }
