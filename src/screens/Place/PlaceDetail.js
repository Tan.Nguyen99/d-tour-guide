import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  ScrollView,
  Animated,
  StyleSheet,
  FlatList,
} from "react-native";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Colors, Fonts, Metrics } from "../../themes";
import SiteActions from "../../redux/SiteRedux/actions";
import { Container, DTText } from "../../components";
import TourActions from "../../redux/BookTourRedux/actions";
import _ from "lodash";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import Icon from "react-native-vector-icons/Ionicons";
import { NavigationUtils } from "../../navigation";

export class PlaceDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      SlideInLeft: new Animated.Value(0),
      SlideInLeftButton: new Animated.Value(0),
      SlideInRight: new Animated.Value(0),
      star: [
        { id: 0, isSelected: false },
        { id: 1, isSelected: false },
        { id: 2, isSelected: false },
        { id: 3, isSelected: false },
        { id: 4, isSelected: false },
      ],
    };
    Animated.parallel([
      Animated.timing(this.state.SlideInLeft, {
        toValue: 1,
        duration: 600,
        useNativeDriver: true,
      }),
      Animated.timing(this.state.SlideInLeftButton, {
        toValue: 1,
        duration: 600,
        useNativeDriver: true,
      }),
      Animated.timing(this.state.SlideInRight, {
        toValue: 1,
        duration: 600,
        useNativeDriver: true,
      }),
    ]).start();
    this.props.getSiteById(this.props.siteId);
  }

  componentDidMount() {
    const { tourId, user } = this.props;

    if (tourId) {
      this.props.getTour(tourId);
    } else {
      let data = {
        tourist_id: user.id,
        tour_guide_id: null,
        promotion_code: null,
        tourist_site_id: null,
        pickup_location: "101B Ngo Quyen",
        start_time: "09:20:00",
        unit_price: 5,
        total_guests: 3,
        status: 0,
      };
      this.props.createCart(data);
    }
  }

  checkStar = (id) => async () => {
    const { site, tour, loading } = this.props;

    const selectStars = this.state.star.map((item) => {
      if (this.state.star[id].isSelected === false) {
        if (item.id <= id) {
          item.isSelected = true;
        }
      } else {
        if (item.id > id) {
          item.isSelected = false;
        }
      }

      return item;
    });
    this.setState({ star: selectStars });
    NavigationUtils.push({
      screen: "FinishTour",
      isBack: true,
      leftButtonsColor: "black",
      title: "Rating",
      color: "white",
      passProps: {
        isPLaceRating: true,
        image: site.image_url,
        star: this.state.star,
        site: site,
      },
    });
  };

  onAddSiteToTour = async () => {
    const { tourId, site, addSiteToTour } = this.props;

    let data = {
      tour_booking_id: tourId,
      tourist_site_id: site.id,
    };
    console.log("data", this.props);
    await addSiteToTour(data);
  };

  static options = () => ({
    topBar: {
      noBorder: true,
      visible: true,
      background: {
        color: "transparent",
      },
      elevation: 0,
    },
  });

  render() {
    const { site, tour, loading } = this.props;
    console.log("loading", loading);

    return (
      <Container scrollEnabled>
        <View style={{ width: "100%", height: 250 }}>
          <Image
            source={{ uri: site.image_url }}
            style={{ width: "100%", height: "100%" }}
            resizeMode={"cover"}
          />
          <Image
            source={require("../../assets/img/bwBackround.jpg")}
            style={{ width: "100%", height: 500, opacity: 0.12 }}
            resizeMode={"cover"}
          />
        </View>
        <Animated.View
          style={{
            transform: [
              {
                translateX: this.state.SlideInLeft.interpolate({
                  inputRange: [0, 1],
                  outputRange: [300, 0],
                }),
              },
            ],
          }}
        >
          <View style={styles.titleContainer}>
            <Text style={styles.title}>{site.name}</Text>
            <Text style={styles.subTitle}>{site.address}</Text>
            <View
              style={{ width: (Metrics.screenWidth / 5) * 4, marginTop: 10 }}
            >
              <Text style={{ textAlign: "justify" }}>{site.description}</Text>
            </View>
          </View>
        </Animated.View>
        <Animated.View
          style={{
            transform: [
              {
                translateX: this.state.SlideInLeftButton.interpolate({
                  inputRange: [0, 1],
                  outputRange: [300, 0],
                }),
              },
            ],
          }}
        >
          <TouchableWithoutFeedback
            onPress={this.onAddSiteToTour}
            style={styles.buttonAddCartContainer}
          >
            <View
              style={{
                alignItems: "center",
                flexDirection: "row",
                height: 90,
              }}
            >
              <Text
                style={{
                  color: Colors.white,
                  fontSize: Fonts.fontSize.xMedium,
                }}
              >
                Add To Tour
              </Text>
              <View style={styles.whiteLine} />
            </View>
          </TouchableWithoutFeedback>
        </Animated.View>
        {!this.state.isRating && (
          <View
            style={{
              justifyContent: "center",
              flex: 1,
              marginTop: 12,
              alignItems: "center",
            }}
          >
            <DTText type="regular16">Rating for this place</DTText>
            <View style={{ flexDirection: "row" }}>
              {this.state.star.map((item) => {
                return (
                  <TouchableWithoutFeedback onPress={this.checkStar(item.id)}>
                    <View
                      style={{
                        width: 27,
                        height: 27,
                        justifyContent: "center",
                        alignItems: "center",
                        borderRadius: 50,
                        backgroundColor: item.isSelected
                          ? Colors.warning
                          : null,
                        marginHorizontal: 5,
                        borderColor: Colors.warning,
                        borderWidth: 1,
                        marginTop: 5,
                      }}
                    >
                      <Icon
                        name={item.isSelected ? "ios-star" : "ios-star-outline"}
                        color={item.isSelected ? Colors.white : Colors.warning}
                      />
                    </View>
                  </TouchableWithoutFeedback>
                );
              })}
            </View>
          </View>
        )}

        <View style={{ flex: 3, paddingLeft: 20 }}>
          <Animated.View
            style={{
              transform: [
                {
                  translateX: this.state.SlideInLeftButton.interpolate({
                    inputRange: [0, 1],
                    outputRange: [-300, 0],
                  }),
                },
              ],
            }}
          >
            <View style={{ flexDirection: "row" }}>
              <View style={styles.primaryLine} />
              <View style={{ marginBottom: 30 }}>
                <Text style={styles.overViewText}>Reviews</Text>
                <FlatList
                  showsHorizontalScrollIndicator={false}
                  data={_.orderBy(site.comments, ["id"], ["desc", "asc"])}
                  contentContainerStyle={{ marginTop: 12 }}
                  renderItem={({ item }) => {
                    return (
                      <View
                        style={{
                          height: 80,
                          // backgroundColor: Colors.,
                          marginBottom: 12,
                          flexDirection: "row",
                          borderBottomColor: Colors.divider,
                          borderBottomWidth: 1,
                        }}
                      >
                        <Image
                          source={require("../../assets/img/usertan.jpg")}
                          style={{
                            width: 60,
                            height: 60,
                            borderRadius: 40,
                            marginRight: 16,
                          }}
                        />
                        <View
                          style={{
                            width: (Metrics.screenWidth / 3) * 2 - 20,
                          }}
                        >
                          <View style={{ flexDirection: "row" }}>
                            {this.state.star.map((star) => {
                              return (
                                <View
                                  style={{
                                    width: 27,
                                    height: 27,
                                    justifyContent: "center",
                                    alignItems: "center",
                                    marginLeft: 5,
                                    marginTop: 5,
                                  }}
                                >
                                  <Icon
                                    name={
                                      star.id + 1 <= item.rating
                                        ? "ios-star"
                                        : "ios-star-outline"
                                    }
                                    color={
                                      star.isSelected
                                        ? Colors.white
                                        : Colors.warning
                                    }
                                    size={16}
                                  />
                                </View>
                              );
                            })}
                          </View>
                          <Text
                            lineBreakMode={"tail"}
                            numberOfLines={2}
                            style={{ marginLeft: 5 }}
                          >
                            {item.content}
                          </Text>
                        </View>
                      </View>
                    );
                  }}
                  keyExtractor={(item) => item.name}
                />
              </View>
            </View>
          </Animated.View>
        </View>
      </Container>
    );
  }
}

const mapStateToProps = (state, props) => ({
  loading: state.tour.addSiteToTourLoading,
  site: state.site.siteById,
  tour: state.tour,
  tourId: state.tour.tourId,
  user: state.login.user,
});

const mapDispatchToProps = (dispatch) => ({
  getSiteById: (id) => dispatch(SiteActions.getSiteById(id)),
  createCart: (data) => dispatch(TourActions.createTour(data)),
  addSiteToTour: (data) => dispatch(TourActions.addSiteToTour(data)),
  getTour: (data) => dispatch(TourActions.getTour(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(PlaceDetail);
const styles = StyleSheet.create({
  titleContainer: {
    width: 800,
    backgroundColor: Colors.white,
    borderRadius: 40,
    marginLeft: 35,
    shadowOffset: { width: 1, height: 1 },
    shadowColor: "black",
    backgroundColor: "white",
    shadowOpacity: 0.2,
    elevation: 6,
    padding: 25,
    paddingBottom: 45,
  },
  title: {
    fontSize: Fonts.fontSize.title,
    fontWeight: Fonts.fontWeight.bold,
  },
  buttonAddCartContainer: {
    height: 80,
    width: 300,
    backgroundColor: Colors.primary,
    marginTop: -40,
    marginLeft: 220,
    borderRadius: 70,
    alignItems: "center",
    flexDirection: "row",
    paddingLeft: 25,
    elevation: 3,
  },
  whiteLine: {
    width: 100,
    height: 1,
    backgroundColor: "white",
    marginLeft: 10,
  },
  primaryLine: {
    width: 20,
    height: 2,
    backgroundColor: Colors.primary,
    marginRight: 10,
    marginTop: 15,
  },
  overViewText: {
    fontSize: Fonts.fontSize.subtitle,
    fontWeight: Fonts.fontWeight.bold,
    color: Colors.primary,
  },
  subTitle: {
    color: Colors.primary,
    fontSize: Fonts.fontSize.medium,
    fontWeight: "bold",
    width: Metrics.screenWidth - 120,
  },
});
