import React, { Component } from "react";
import {
  Text,
  View,
  TouchableWithoutFeedback,
  TextInput,
  Image,
  FlatList,
  StyleSheet,
  KeyboardAvoidingView,
  Keyboard,
  ScrollView,
  ActivityIndicator,
} from "react-native";
import { Navigation } from "react-native-navigation";
import Icon from "react-native-vector-icons/Ionicons";
import { connect } from "react-redux";
import LoginActions from "../redux/AuthRedux/actions";
import { Colors, Fonts } from "../themes";
import { fontWeight } from "../themes/Fonts";
import Icons from "react-native-vector-icons/FontAwesome";
import { NavigationUtils } from "../navigation";
import { iconsMap } from "../utils/appIcons";
import SiteActions from "../redux/SiteRedux/actions";
import { Container } from "../components";
import AsyncStorage from "@react-native-community/async-storage";
import LottieView from "lottie-react-native";
import { fcmService } from "../utils/FCMService";
import { localNotificationService } from "../utils/LocalNotificationService";
import { getAllPlaceApi, searchPLaceApi } from "../api/site";
import TourActions from "../redux/BookTourRedux/actions";

class Home extends Component {
  constructor(props) {
    super(props);
    this.props.getAllSite();
    this.state = {
      CATEGORY_ICON: {
        Entertainment: "ios-star-outline",
        Drink: "ios-beer",
        Supermarket: "ios-pizza",
        Attraction: "ios-boat",
        Motel: "ios-boat",
        Others: "ios-boat",
      },
      listPopular: [],
      imageLoading: true,
      loading: false,
      star: [
        { id: 0, isSelected: false },
        { id: 1, isSelected: false },
        { id: 2, isSelected: false },
        { id: 3, isSelected: false },
        { id: 4, isSelected: false },
      ],
      searchValue: " ",
    };
    // this.receiveFirebaseNotification();
    this.getPopular();
    if (this.props.tourData !== null) {
      this.props.getTour(this.props.tourId);
    }
  }

  static options = () => ({
    topBar: {
      noBorder: true,
      visible: true,
      title: {
        text: "D-TRAVEL",
        alignment: "center",
        color: Colors.white,
        fontSize: Fonts.fontSize.xMedium,
        fontWeight: Fonts.fontWeight.bold,
      },
      background: {
        color: "#51cb96",
      },
      elevation: 0,
    },
  });

  getPopular = async () => {
    this.setState({ loading: true });
    const response = await getAllPlaceApi();
    this.setState({ listPopular: response.data });
    this.setState({ loading: false });
  };

  pushListSites(tourist_sites) {
    NavigationUtils.push({
      screen: "ListPlaces",
      isBack: true,
      isTopBarEnable: true,
      noBorder: true,
      leftButtonsColor: Colors.black,
      rightButtons: true,
      rightButtonsColor: Colors.black,
      passProps: {
        categorySites: tourist_sites,
      },
    });
  }

  onHandleTour = () => {
    const { tourData, user } = this.props;
    if (tourData === null) {
      this.pushCart();
    } else {
      if (tourData.status === 1) {
        NavigationUtils.push({
          screen: "WaitingTour",
          isBack: true,
          isDrawBehind: true,
          isTopBarEnable: true,
          leftButtonsColor: "black",
          color: "white",
          title: "Tour Process",
        });
      }

      if (
        tourData.status === 0 ||
        tourData.status === 3 ||
        tourData.status === 4
      ) {
        this.pushCart();
      }
      if (tourData.status === 2) {
        NavigationUtils.push({
          screen: "TrackingTour",
          isBack: false,
          isDrawBehind: true,
          color: "white",
          title: "Tour Process",
          leftButtons: [
            {
              id: "backToRoot",
              icon: iconsMap["chevron-left"],
              color: "black",
            },
          ],
        });
      }
    }
  };

  pushPlaceDetail = (item) => {
    NavigationUtils.push({
      screen: "PlaceDetail",
      isBack: true,
      isTopBarEnable: true,
      noBorder: true,
      leftButtonsColor: Colors.white,
      passProps: {
        siteId: item.id,
      },
    });
  };
  pushTest = () => {
    this.props.logout();
  };
  pushCart = () => {
    NavigationUtils.push({
      screen: "Cart",
      isBack: true,
      title: "Tour",
      isTopBarEnable: true,
      noBorder: true,
      color: "white",
      isDrawBehind: true,
      leftButtonsColor: Colors.black,
    });
  };

  onProcessTour = () => {
    // Navigation.mergeOptions(this.props.componentId, {
    //   bottomTabs: {
    //     currentTabIndex: 1,
    //   },
    // });
    NavigationUtils.push({
      screen: "WaitingTour",
      isTopBarEnable: true,
      isBack: true,
      color: "white",
      leftButtonsColor: Colors.black,
      title: "Tour tracking",
    });
  };

  onSearch = async () => {
    try {
      const response = await searchPLaceApi({
        searchValue: this.state.searchValue,
      });
      this.pushListSites(response.data);
      this.setState({ searchValue: " " });
    } catch (error) {
      console.log("error", error);
    }
  };

  getSearchValue = (text) => () => {
    this.setState({ searchValue: text });
  };

  renderCategoryItem = ({ item }) => {
    let categoryArray = item.name.trim().split(" ");
    let IconName = categoryArray[categoryArray.length - 1];
    return (
      <TouchableWithoutFeedback
        onPress={() => this.pushListSites(item.tourist_sites)}
      >
        <View
          style={{
            backgroundColor: "#51cb96",
            width: 100,
            height: 110,
            marginRight: 20,
            alignItems: "center",
            justifyContent: "center",
            borderRadius: 15,
          }}
        >
          <Icon
            name={this.state.CATEGORY_ICON[IconName]}
            size={55}
            color={Colors.white}
            style={{ marginHorizontal: 25, marginVertical: 3 }}
          />
          <Text
            style={{ color: "white", alignSelf: "center", textAlign: "center" }}
          >
            {item.name}
          </Text>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  renderPopularSiteItem = ({ item }) => {
    return (
      <TouchableWithoutFeedback onPress={() => this.pushPlaceDetail(item)}>
        <View
          style={{
            alignItems: "center",
            marginRight: 15,
            borderRadius: 9,
            shadowOffset: { width: 3, height: 4 },
            shadowColor: "black",
            backgroundColor: "white",
            shadowOpacity: 0.3,
            elevation: 10,
            width: 150,
            height: 250,
            paddingTop: 5,
          }}
        >
          <View
            style={{
              overflow: "hidden",
              paddingHorizontal: 4,
              justifyContent: "center",
              alignItems: "center",
              height: 160,
              marginBottom: 12,
            }}
          >
            <Image
              source={{ uri: item.image }}
              style={{
                borderRadius: 10,
                width: 140,
                height: 160,
                resizeMode: "cover",
                overflow: "hidden",
              }}
              // onLoadStart={() => {
              //   this.setState({ imageLoading: true });
              // }}
              onLoadEnd={() => {
                this.setState({ imageLoading: false });
              }}
            />
            {this.state.imageLoading && (
              <ActivityIndicator
                size="large"
                color={Colors.primary}
                style={{ position: "absolute" }}
              />
            )}
          </View>
          <View
            style={{
              flex: 1,
              width: "100%",
            }}
          >
            <View style={{ flexDirection: "row" }}>
              {this.state.star.map((item) => {
                return (
                  <View
                    style={{
                      width: 20,
                      height: 20,
                      justifyContent: "center",
                      alignItems: "center",
                      borderRadius: 50,
                      backgroundColor: Colors.warning,
                      marginLeft: 5,
                    }}
                  >
                    <Icon name="ios-star" color={Colors.white} />
                  </View>
                );
              })}
            </View>
            <Text
              style={{
                fontWeight: Fonts.fontWeight.medium,
                marginLeft: 5,
                marginTop: 5,
              }}
            >
              {item.name}
            </Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  render() {
    const { sitesData, getAllSiteLoading } = this.props;

    // const DATA = sitesData.map((item) => {
    //   let category = {
    //     id: item.id,
    //     category: item.name,
    //     tourist_sites: item.tourist_sites,
    //   };
    //   return category;
    // });

    const SITE = [
      {
        id: "bd7acbea-c1b1-46c2-aed5-3ad53abb28ba",
        name: "Cruise",
        image: require("../assets/img/site3.jpg"),
      },
      {
        id: "3ac68afc-c605-48d3-a4f8-fbd91aa97f63",
        name: "Plane",
        image: require("../assets/img/site2.jpg"),
      },
      {
        id: "58694a0f-3da1-471f-bd96-145571e29d72",
        name: "Train",
        image: require("../assets/img/site1.jpg"),
      },
      {
        id: "58694a0f-3da1-471f-bd96-145571e2972",
        name: "Beer",
        image: require("../assets/img/site3.jpg"),
      },
    ];

    return (
      <Container loading={getAllSiteLoading || this.state.loading}>
        <View style={{ flex: 1 }}>
          <ScrollView>
            <View
              style={{
                backgroundColor: Colors.primary,
                paddingHorizontal: 20,
                borderBottomEndRadius: 20,
                borderBottomStartRadius: 20,
              }}
            >
              <Text
                style={{
                  fontSize: Fonts.fontSize.title,
                  color: Colors.white,
                  fontWeight: Fonts.fontWeight.bold,
                }}
              >
                Place in Da Nang
              </Text>

              <View
                style={{
                  flexDirection: "row",
                  backgroundColor: "white",
                  height: 50,
                  borderRadius: 25,
                  marginVertical: 10,
                }}
              >
                <View style={{ justifyContent: "center", flex: 1 }}>
                  <TextInput
                    placeholder="Eg. Dragon Bridge, Linh Ung Pagoda"
                    onChangeText={(text) => this.getSearchValue(text)}
                    style={{ paddingLeft: 20 }}
                  />
                </View>
                <TouchableWithoutFeedback onPress={this.onSearch}>
                  <View style={{ justifyContent: "center" }}>
                    <Icons
                      name="search"
                      size={25}
                      color={Colors.divider}
                      style={{ marginHorizontal: 25 }}
                    />
                  </View>
                </TouchableWithoutFeedback>
              </View>
            </View>

            <View style={{ marginLeft: 15, marginTop: 10 }}>
              <View>
                <Text
                  style={{
                    fontSize: Fonts.fontSize.xMedium,
                    fontWeight: Fonts.fontWeight.bold,
                  }}
                >
                  CHOOSE BY CATEGORIES
                </Text>
                <View style={{ marginTop: 15 }}>
                  <FlatList
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    data={sitesData}
                    renderItem={this.renderCategoryItem}
                    keyExtractor={(item) => item.name}
                  />
                </View>
              </View>
              <View style={{ marginTop: 30 }}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}
                >
                  <Text
                    style={{
                      fontSize: Fonts.fontSize.xMedium,
                      fontWeight: Fonts.fontWeight.bold,
                    }}
                  >
                    MOST POPULAR
                  </Text>
                  <TouchableWithoutFeedback
                    onPress={() => this.pushListSites(this.state.listPopular)}
                  >
                    <View style={{ marginRight: 10 }}>
                      <Text
                        style={{
                          fontSize: Fonts.fontSize.xMedium,
                          fontWeight: Fonts.fontWeight.bold,
                          color: Colors.primary,
                        }}
                      >
                        See more
                      </Text>
                    </View>
                  </TouchableWithoutFeedback>
                </View>
                <View
                  style={{
                    // height: 260,
                    marginBottom: 25,
                    // paddingBottom: 15,
                  }}
                >
                  <FlatList
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    contentContainerStyle={{
                      paddingVertical: 15,
                    }}
                    data={this.state.listPopular}
                    renderItem={this.renderPopularSiteItem}
                    keyExtractor={(item) => item.name}
                  />
                </View>
              </View>
            </View>
          </ScrollView>
          <TouchableWithoutFeedback onPress={this.onHandleTour}>
            <View
              style={{
                backgroundColor: Colors.primary,
                position: "absolute",
                width: 56,
                height: 56,
                borderRadius: 40,
                justifyContent: "center",
                alignItems: "center",
                bottom: 18,
                right: 18,
              }}
            >
              {this.props.isCartAvailable ? (
                <Icons name="shopping-cart" color={Colors.white} size={24} />
              ) : (
                <LottieView
                  source={require("../assets/lottieFile/lf30_editor_8S5jzF.json")}
                  autoPlay
                  style={{ width: 70, height: 70 }}
                  loop
                />
              )}
            </View>
          </TouchableWithoutFeedback>
        </View>
      </Container>
    );
  }
}
const mapStateToProps = (state, props) => ({
  getAllSiteLoading: state.site.getAllSiteLoading,
  sitesData: state.site.sites,
  isCartAvailable: state.tour.isCartAvailable,
  tourData: state.tour.data,
  tourId: state.tour.tourId,
  user: state.login,
});

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(LoginActions.logout()),
  getAllSite: () => dispatch(SiteActions.getAllSite()),
  createCart: (data) => dispatch(TourActions.createTour(data)),
  getTour: (data) => dispatch(TourActions.getTour(data)),
});
export default connect(mapStateToProps, mapDispatchToProps)(Home);
const styles = StyleSheet.create({
  shadowContainer: {
    height: 180,
    width: 150,
    elevation: 10,
    shadowColor: "black",
    shadowOffset: { width: 3, height: 4 },
    shadowOpacity: 0.3,
    shadowRadius: 4,
    // backgroundColor: "pink",
    marginRight: 10,
    borderRadius: 10,
  },
});
