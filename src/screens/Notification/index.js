import React, { Component } from "react";
import {
  View,
  Text,
  TouchableWithoutFeedback,
  SafeAreaView,
  Platform,
  Image,
  FlatList,
} from "react-native";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import AppActions from "../../redux/AppRedux/actions";
import { NavigationUtils } from "../../navigation";
import CheckActions from "../../redux/ChekingRedux/actions";
import MapView, {
  PROVIDER_GOOGLE,
  Marker,
  Callout,
  Polygon,
  Polyline,
} from "react-native-maps";
// import MapViewDirections from "react-native-maps-directions";
import Geolocation from "@react-native-community/geolocation";
import { request, PERMISSIONS } from "react-native-permissions";
import RNAndroidLocationEnabler from "react-native-android-location-enabler";
import { Colors, Fonts, Metrics } from "../../themes";
import { DTText } from "../../components";
import { getNotificationApi } from "../../api/auth";
import { Navigation } from "react-native-navigation";

export class Notification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      initialRegion: {
        latitude: 16.075669,
        longitude: 108.234441,
        latitudeDelta: 0.015,
        longitudeDelta: 0.0121,
      },
      markers: [],
      listNotification: [],
    };
  }

  getNotification = async () => {
    const response = await getNotificationApi();
    console.log("data", response);
    this.setState({ listNotification: response.data });
  };

  componentDidMount() {
    this.requestLocationPermission();
    this.getNotification();
  }
  requestLocationPermission = async () => {
    if (Platform.OS === "ios") {
      var response = await request(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE);
      if (response === "granted") {
        this.locationCurrentPosition();
      }
    } else {
      var response = await request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION);
      if (response === "granted") {
        this.getPermissions();
      }
    }
  };

  onNotificationDetail() {
    console.log("vodmdmdmdm");
    NavigationUtils.push({
      screen: "NotificationDetail",
      isBack: true,
      title: "Tour",
      isTopBarEnable: true,
      noBorder: true,
      color: "white",
      isDrawBehind: true,
      leftButtonsColor: Colors.black,
    });
  }
  locationCurrentPosition = () => {
    Geolocation.getCurrentPosition(
      (position) => {
        console.log("vooo123123", position);
        let region = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: 0.015,
          longitudeDelta: 0.0121,
        };
        this.setState({ initialRegion: region });
        this.props.saveCurrentLocation(region);
      },
      (error) => Alert.alert(error.message),
      {
        enableHighAccuracy: false,
        timeout: 10000,
      }
    );
  };

  getPermissions = () => {
    RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
      interval: 10000,
      fastInterval: 5000,
    }).then((data) => {
      if (data === "already-enabled") {
        this.locationCurrentPosition();
      } else {
        setTimeout(() => {
          this.locationCurrentPosition();
        }, 1000);
      }
    });
  };
  renderItem = ({ item }) => {
    return (
      <TouchableWithoutFeedback
        onPress={() => this.onNotificationDetail(item.id)}
      >
        <View
          style={{
            flexDirection: "row",
            paddingHorizontal: 20,
            paddingBottom: 6,
            borderBottomColor: Colors.divider,
            borderBottomWidth: 1,
          }}
        >
          <Image
            source={{ uri: item.image }}
            style={{ width: 90, height: 90 }}
          />
          <View style={{ marginLeft: 12, justifyContent: "space-around" }}>
            <DTText type="buttonRegular12">{item.title}</DTText>
            <DTText
              type="buttonRegular12"
              lineBreakMode={"tail"}
              numberOfLines={1}
              style={{ width: Metrics.screenWidth / 2 }}
            >
              You have a new promotion from D Travel
            </DTText>
            <DTText type="buttonRegular12" color={Colors.primary}>
              {item.body}
            </DTText>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  render() {
    console.log("id nef con dix", this.props.componentId);

    if (this.state.listNotification.length === 0) {
      return (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <View
            style={{
              alignSelf: "center",
              alignItems: "center",
              justifyContent: "center",
              opacity: 0.5,
            }}
          >
            <Image
              source={require("../../assets/img/empty.png")}
              style={{ width: 180, height: 180 }}
            />
            <DTText type="bodyRegular16">Don't have any notification</DTText>
          </View>
        </View>
      );
    }
    return (
      <View style={{ flex: 1 }}>
        <View
          style={{
            alignItems: "center",
            height: 55,
            justifyContent: "center",
            borderBottomColor: Colors.neutralDarkBlack80,
            borderBottomWidth: 0.5,
            marginBottom: 12,
          }}
        >
          <DTText type="h4">Notification</DTText>
        </View>
        <FlatList
          showsHorizontalScrollIndicator={false}
          data={this.state.listNotification}
          renderItem={this.renderItem}
          keyExtractor={(item) => item.name}
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  checkData: state.check.CheckListData,
});

const mapDispatchToProps = (dispatch) => ({
  saveCurrentLocation: (data) => {
    dispatch(AppActions.getCurrentLocation(data));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Notification);
