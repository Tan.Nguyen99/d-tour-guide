import React, { Component, useEffect } from "react";
import {
  Text,
  View,
  Image,
  TextInput,
  StyleSheet,
  TouchableWithoutFeedback,
  ScrollView,
  FlatList,
  Dimensions,
  ImageBackground,
  Alert,
} from "react-native";
import LoginActions from "../../redux/AuthRedux/actions";
import colors from "../../themes/Colors";
import { Container, DTText } from "../../components";
import metrics from "../../themes/Metrics";
import { Colors, Metrics } from "../../themes";
import Icon from "react-native-vector-icons/Ionicons";
import { useSelector, useDispatch } from "react-redux";
import { fcmService } from "../../utils/FCMService";
import { localNotificationService } from "../../utils/LocalNotificationService";
import { NavigationUtils } from "../../navigation";
import messaging from "@react-native-firebase/messaging";
import firebase from "@react-native-firebase/app";
import Geolocation from "@react-native-community/geolocation";
import RNAndroidLocationEnabler from "react-native-android-location-enabler";
import AppAction from "../../redux/AppRedux/actions";
import TourActions from "../../redux/BookTourRedux/actions";
import { iconsMap } from "../../utils/appIcons";
const { height, width } = Dimensions.get("window");

const Profile = () => {
  const dispatch = useDispatch();
  const userData = useSelector((state) => state.login.data);
  const touId = useSelector((state) => state.tour.tourId);
  const onLogout = async () => {
    await dispatch(LoginActions.logout());
  };

  const onNotification = async (notify) => {
    await dispatch(TourActions.getTour(touId));
    if (JSON.parse(notify.data.status) === 1) {
      NavigationUtils.push({
        screen: "WaitingTour",
        isBack: true,
        isDrawBehind: true,
        isTopBarEnable: true,
        leftButtonsColor: "black",
        color: "white",
        title: "Tour Process",
      });
    }

    if (JSON.parse(notify.data.status) === 2) {
      NavigationUtils.push({
        screen: "TrackingTour",
        isBack: false,
        isDrawBehind: true,
        color: "white",
        title: "Tour Process",
        leftButtons: [
          {
            id: "backToRoot",
            icon: iconsMap["chevron-left"],
            color: "black",
          },
        ],
      });
    }

    if (JSON.parse(notify.data.status) === 4) {
      NavigationUtils.push({
        screen: "FinishTour",
        isBack: true,
        isDrawBehind: true,
        isTopBarEnable: true,
        leftButtonsColor: "black",
        title: "Tour Process",
      });
      // let data = {
      //   tourist_id: user.id,
      //   tour_guide_id: null,
      //   promotion_code: null,
      //   tourist_site_id: null,
      //   pickup_location: "101B Ngo Quyen",
      //   start_time: "09:20:00",
      //   unit_price: 5,
      //   total_guests: 3,
      //   status: 0,
      // };
      // this.props.createCart(data);
    }
    const options = {
      soundName: "default",
      playSound: true,
    };
    localNotificationService.showNotification(
      0,
      notify.notification.title,
      notify.notification.body,
      notify.notification,
      options
    );
  };

  const receiveFirebaseNotification = async () => {
    fcmService.registerAppWithFCM();
    fcmService.register(onRegister, onNotification, onOpenNotification);
    localNotificationService.configure(onOpenNotification);
    function onRegister(token) {
      console.log("registerToken", token);
    }

    function onOpenNotification(notify) {
      console.log("onOpenNotification", notify);
      Alert.alert("open notification", notify.body);
      NavigationUtils.push({ screen: "Cart" });
    }
    return () => {
      console.log("unRegister");
      fcmService.unRegister();
      localNotificationService.unRegister;
    };
  };

  useEffect(() => {
    receiveFirebaseNotification();
  }, []);

  useEffect(() => {
    requestLocationPermission();
  }, []);
  const requestLocationPermission = async () => {
    if (Platform.OS === "ios") {
      var response = await request(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE);
      if (response === "granted") {
        locationCurrentPosition();
      }
    } else {
      var response = await request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION);
      if (response === "granted") {
        getPermissions();
      }
    }
  };

  const locationCurrentPosition = () => {
    Geolocation.getCurrentPosition(
      async (position) => {
        console.log("vooooo", position);
        let region = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: 0.015,
          longitudeDelta: 0.0121,
        };
        // setState({ initialRegion: region });
        // setCurrentLocation(region);
        await dispatch(AppAction.getCurrentLocation(region));
      },
      (error) => Alert.alert(error.message),
      {
        enableHighAccuracy: false,
        timeout: 10000,
      }
    );
  };

  const getPermissions = () => {
    RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
      interval: 10000,
      fastInterval: 5000,
    }).then((data) => {
      if (data === "already-enabled") {
        locationCurrentPosition();
      } else {
        setTimeout(() => {
          locationCurrentPosition();
        }, 1000);
      }
    });
  };

  return (
    <Container>
      <Image
        source={require("../../assets/img/forest_trees.jpg")}
        style={{
          width: metrics.screenWidth,
          height: metrics.screenHeight / 3 - 24,
          // backgroundColor: Colors.white,
          position: "absolute",
          opacity: 0.8,
          borderBottomLeftRadius: 32,
          borderBottomRightRadius: 32,
        }}
      />
      <View
        style={{
          flexDirection: "row",
          justifyContent: "flex-end",
        }}
      >
        <TouchableWithoutFeedback
          onPress={() => {
            onLogout();
          }}
        >
          <View
            style={{
              width: 36,
              height: 36,
              backgroundColor: Colors.white,
              alignItems: "center",
              justifyContent: "center",
              borderRadius: 20,
              marginTop: 12,
              marginRight: 12,
              borderWidth: 1,
              borderColor: "white",
            }}
          >
            <Icon
              // name="ios-log-out"
              name="ios-settings"
              size={24}
              color={Colors.primary}
            />
          </View>
        </TouchableWithoutFeedback>
      </View>
      <View
        style={{
          width: metrics.screenWidth,
          height: metrics.screenHeight / 3 - 24,
          alignItems: "center",
          justifyContent: "flex-end",
        }}
      >
        <Image
          source={require("../../assets/img/usertan.jpg")}
          style={{
            width: 128,
            height: 128,
            borderRadius: 200,
            borderWidth: 3,
            borderColor: "white",
          }}
        />
      </View>
      <View style={{ paddingHorizontal: 20 }}>
        <View
          style={{
            borderRadius: 1,
            borderBottomWidth: 1,
            borderColor: Colors.divider,
            marginBottom: 8,
          }}
        >
          <DTText type="bodyRegular16" color={Colors.primaryDark}>
            Full name
          </DTText>
          <TextInput
            value={userData.full_name}
            style={{
              width: Metrics.screenWidth - 40,
              height: 40,
              fontSize: 16,
            }}
            editable={false}
          />
        </View>

        <View
          style={{
            borderRadius: 1,
            borderBottomWidth: 1,
            borderColor: Colors.divider,
            marginBottom: 8,
          }}
        >
          <DTText type="bodyRegular16" color={Colors.primaryDark}>
            Email
          </DTText>
          <TextInput
            style={{
              width: Metrics.screenWidth - 40,
              height: 40,
              fontSize: 16,
            }}
            editable={false}
            value={userData.email}
          />
        </View>

        <View
          style={{
            borderRadius: 1,
            borderBottomWidth: 1,
            borderColor: Colors.divider,
            marginBottom: 8,
          }}
        >
          <DTText type="bodyRegular16" color={Colors.primaryDark}>
            Phone number
          </DTText>
          <TextInput
            style={{
              width: Metrics.screenWidth - 40,
              height: 40,
              fontSize: 16,
            }}
            value={userData.phone_number}
            editable={false}
          />
        </View>

        <View
          style={{
            borderRadius: 1,
            borderBottomWidth: 1,
            borderColor: Colors.divider,
            marginBottom: 8,
          }}
        >
          <DTText type="bodyRegular16" color={Colors.primaryDark}>
            Address
          </DTText>
          <TextInput
            style={{
              width: Metrics.screenWidth - 40,
              height: 40,
              fontSize: 16,
            }}
            editable={false}
            value={userData.address || "Update information"}
          />
        </View>

        <TouchableWithoutFeedback onPress={onLogout}>
          <View
            style={{
              borderRadius: 1,
              borderBottomWidth: 1,
              borderColor: Colors.divider,
              marginBottom: 8,
            }}
          >
            <DTText
              type="labelTitle14"
              color={Colors.primaryDark}
              style={{ paddingBottom: 30 }}
            >
              Log out
            </DTText>
          </View>
        </TouchableWithoutFeedback>
      </View>
    </Container>
  );
};

export default Profile;

const styles = StyleSheet.create({
  activityButton: {
    backgroundColor: colors.primary,
    borderRadius: 5,
    paddingHorizontal: 10,
    paddingVertical: 3,
  },
  activityTitle: { color: colors.white, fontSize: 15 },
});
