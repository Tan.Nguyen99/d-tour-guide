import React, { useRef, useState, useEffect } from "react";
import { View, StyleSheet, Alert, Text, TouchableOpacity } from "react-native";
import { DTText, Container } from "../../components";
import { Colors, Fonts } from "../../themes";
import { useDispatch, useSelector } from "react-redux";
import { NavigationUtils } from "../../navigation";
import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from "react-native-confirmation-code-field";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import { getVerifyCodeApi, verifyEmailApi } from "../../api/auth";
import AppActions from "../../redux/AppRedux/actions";

const VerifyCode = (propsData) => {
  const dispatch = useDispatch();
  const [isConfirmCode, setIsConfirmCode] = useState(false);
  const userId = useSelector((state) => state.login.user.id);

  const CELL_COUNT = 6;
  const [value, setValue] = useState("");
  const [confirmCodeEmail, setConfirmCodeEmail] = useState("");
  const [loading, setLoading] = useState(false);
  const ref = useBlurOnFulfill({ value, cellCount: CELL_COUNT });
  const [props, getCellOnLayoutHandler] = useClearByFocusCell({
    value,
    setValue,
  });

  const onGetVerifyCode = async () => {
    let data = {
      user_id: userId,
    };

    try {
      setLoading(true);
      const response = await getVerifyCodeApi(data);
      console.log("vooo", response);
      setLoading(false);
      Alert.alert("Success", "Code have been sent. Please check your email");
    } catch (error) {
      Alert.alert("Error", "Some thing when wrong");
      setLoading(false);
    }
  };

  const onVerifyPasswordCode = async () => {
    setLoading(true);
    if (value.length < 6) {
      Alert.alert("Error", "Code invalid");
      setLoading(false);
    } else {
      try {
        let data = {
          user_id: userId,
          verify_code: value,
        };
        const response = await verifyEmailApi(data);
        await dispatch(AppActions.startup());
      } catch (error) {
        setLoading(false);
        Alert.alert("Error", error.message);
      }
    }
  };

  return (
    <Container style={{ marginTop: 55 }} loading={loading}>
      <View style={styles.container}>
        <View style={styles.topView}>
          <DTText type="bold24" marginBottom={10} marginTop={20}>
            Verify Email
          </DTText>
          <DTText
            type="regular14"
            color={Colors.greyText}
            style={styles.textTitle}
            marginBottom={50}
          >
            A code has been sent to your registered email, please open your
            inbox to check.
          </DTText>

          <View>
            <CodeField
              ref={ref}
              {...props}
              value={value}
              onChangeText={setValue}
              cellCount={CELL_COUNT}
              rootStyle={styles.codeFieldRoot}
              keyboardType="number-pad"
              textContentType="oneTimeCode"
              renderCell={({ index, symbol, isFocused }) => (
                <Text
                  key={index}
                  style={[styles.cell, isFocused && styles.focusCell]}
                  onLayout={getCellOnLayoutHandler(index)}
                >
                  {symbol || (isFocused ? <Cursor /> : null)}
                </Text>
              )}
            />
            <View style={styles.resendCodeContainer}>
              <DTText type="regular14" marginRight={5} color={Colors.greyText}>
                Haven’t received the code?
              </DTText>
              {/* <TouchableText
                  title="Send it again"
                  textType={"regular14"}
                  style={styles.touchableText}
                  onPress={
                    propsData.isSignUpVerify
                      ? onResentVerifyEmail
                      : formik.handleSubmit
                  }
                /> */}
              <TouchableWithoutFeedback onPress={onGetVerifyCode}>
                <DTText type="regular14" marginRight={5} color={Colors.primary}>
                  Send it again
                </DTText>
              </TouchableWithoutFeedback>
            </View>
          </View>
        </View>
        <View style={styles.bottomView}>
          <TouchableOpacity onPress={onVerifyPasswordCode}>
            <View
              style={{
                width: "100%",
                height: 48,
                backgroundColor: "#51cb96",
                borderRadius: 5,
                justifyContent: "center",
                alignItems: "center",
                marginTop: 35,
              }}
            >
              <Text
                style={{
                  color: Colors.white,
                  fontWeight: Fonts.fontWeight.medium,
                  fontSize: Fonts.fontSize.xMedium,
                }}
              >
                Continue
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </Container>
  );
};

export default VerifyCode;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 25,
  },
  textInputContainer: {
    marginBottom: 12,
  },
  btn: {
    marginVertical: 25,
  },
  textTitle: { lineHeight: 25 },
  topView: { flex: 1 },
  bottomView: { flex: 1 },
  codeFieldRoot: { marginTop: 20 },
  cell: {
    width: 45,
    height: 45,
    lineHeight: 38,
    fontSize: 24,
    borderWidth: 1,
    borderColor: Colors.neturalGrey,
    textAlign: "center",
    borderRadius: 5,
    backgroundColor: Colors.neturalLightGrey,
  },
  focusCell: {
    borderColor: "#000",
  },
  resendCodeContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 40,
  },
});
