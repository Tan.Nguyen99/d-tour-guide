import React, { useState, useEffect, useRef } from "react";
import {
  View,
  Text,
  TouchableWithoutFeedback,
  TextInput,
  StyleSheet,
  ImageBackground,
  Image,
  ScrollView,
  SafeAreaView,
  Platform,
} from "react-native";
import PropTypes from "prop-types";
import { useSelector, useDispatch } from "react-redux";
import Icons from "react-native-vector-icons/FontAwesome";
import { Fonts, Colors } from "../../themes";
import { ShadowInput, DTText, Container } from "../../components";
import RegisterActions from "../../redux/AuthRedux/actions";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import DeviceInfo from "react-native-device-info";
import * as Yup from "yup";
import { useFormik } from "formik";
import { NavigationUtils } from "../../navigation";

const TEXT_INPUT_FULL_NAME = "TEXT_INPUT_FULL_NAME";
const TEXT_INPUT_CONFIRM_PASSWORD = "TEXT_INPUT_CONFIRM_PASSWORD";
const TEXT_INPUT_EMAIL = "TEXT_INPUT_EMAIL";
const TEXT_INPUT_PASSWORD = "TEXT_INPUT_PASSWORD";
const TEXT_INPUT_PHONE = "TEXT_INPUT_PHONE";

const Register = () => {
  const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
  const dispatch = useDispatch();
  const registerLoading = useSelector((state) => state.login.registerLoading);
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);

  let emailRef = useRef(null);
  let passwordRef = useRef(null);
  let fullNameRef = useRef(null);
  let confirmPasswordRef = useRef(null);
  let phoneRef = useRef(null);

  const onRegister = async ({ fullName, email, phone, password }) => {
    const data = {
      full_name: fullName,
      email: email,
      password: password,
      confirm_password: password,
      phone_number: phone,
      device_token: DeviceInfo.getUniqueId(),
      accountable_type: 1,
    };
    // this.props.register(data);
    await dispatch(RegisterActions.register(data));
    NavigationUtils.push({
      screen: "VerifyCode",
      title: "Register",
      isBack: true,
      isTopBarEnable: true,
      noBorder: true,
      color: "white",
      leftButtonsColor: Colors.neutralDarkBlack80,
    });
  };

  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
      fullName: "",
      confirmPassword: "",
      phone: "",
    },
    validationSchema: Yup.object({
      fullName: Yup.string().required("Full name is required"),
      email: Yup.string()
        .email("Email is invalid")
        .required("Email is required"),
      phone: Yup.string()
        .matches(phoneRegExp, "Phone number is not valid")
        .required("Phone number is required"),
      password: Yup.string()
        .min(6, "Password is at least 6 characters")
        .max(20, "Password is least than 20 characters")
        .required("Password is required"),
      confirmPassword: Yup.string()
        .min(6, "Password is at least 6 characters")
        .max(20, "Password is least than 20 characters")
        .required("Password is required")
        .oneOf([Yup.ref("password"), null], "Confirm Password not match"),
    }),
    onSubmit: (values) => {
      onRegister(values);
    },
  });

  const onSubmitEditing = (field) => {
    if (field === TEXT_INPUT_FULL_NAME) {
      fullNameRef.current?.focus();
    }
    if (field === TEXT_INPUT_EMAIL) {
      emailRef.current?.focus();
    }
    if (field === TEXT_INPUT_PHONE) {
      phoneRef.current?.focus();
    }
    if (field === TEXT_INPUT_PASSWORD) {
      passwordRef.current?.focus();
    }
    if (field === TEXT_INPUT_CONFIRM_PASSWORD) {
      confirmPasswordRef.current?.blur();
      formik.handleSubmit();
    }
  };

  return (
    <View style={styles.container}>
      <Container loading={registerLoading}>
        <ImageBackground
          source={require("../../assets/img/intro3.jpg")}
          style={styles.image}
        >
          <KeyboardAwareScrollView
            enableResetScrollToCoords={false}
            keyboardShouldPersistTaps="handled"
          >
            <View style={{ alignSelf: "center", paddingVertical: 5 }}>
              <Image
                source={require("../../assets/img/logo.jpg")}
                style={{
                  width: 105,
                  height: 105,
                  borderRadius: 35,
                  marginTop: Platform.OS === "ios" ? 0 : 60,
                }}
              />
            </View>
            <View
              style={{
                alignSelf: "center",
                width: "90%",
                flex: 1,
                backgroundColor: Colors.white,
                borderRadius: 15,
                justifyContent: "space-evenly",
                paddingHorizontal: 15,
                marginVertical: 25,
                paddingVertical: 10,
              }}
            >
              <View
                style={{
                  justifyContent: "space-around",
                  height: 400,
                }}
              >
                {/* from here */}
                <View style={styles.inputContainer}>
                  <View style={{ flexDirection: "row" }}>
                    <View style={{ justifyContent: "center" }}>
                      <Icons
                        name="user"
                        size={18}
                        color={Colors.divider}
                        style={{ marginHorizontal: 10 }}
                      />
                    </View>
                    <View style={{ justifyContent: "center", flex: 1 }}>
                      <TextInput
                        ref={fullNameRef}
                        placeholder="Full name"
                        onChangeText={formik.handleChange("fullName")}
                        onSubmitEditing={() =>
                          onSubmitEditing(TEXT_INPUT_FULL_NAME)
                        }
                      />
                    </View>
                  </View>
                  <DTText
                    marginTop={4}
                    type="buttonRegular12"
                    color={Colors.error}
                  >
                    {formik.errors.fullName}
                  </DTText>
                </View>

                <View style={styles.inputContainer}>
                  <View style={{ flexDirection: "row" }}>
                    <View style={{ justifyContent: "center" }}>
                      <Icons
                        name="envelope"
                        size={18}
                        color={Colors.divider}
                        style={{ marginHorizontal: 10 }}
                      />
                    </View>
                    <View style={{ justifyContent: "center", flex: 1 }}>
                      <TextInput
                        ref={emailRef}
                        placeholder="Email"
                        onChangeText={formik.handleChange("email")}
                        onSubmitEditing={() =>
                          onSubmitEditing(TEXT_INPUT_EMAIL)
                        }
                      />
                    </View>
                  </View>
                  <DTText
                    marginTop={4}
                    type="buttonRegular12"
                    color={Colors.error}
                  >
                    {formik.errors.email}
                  </DTText>
                </View>

                <View style={styles.inputContainer}>
                  <View style={{ flexDirection: "row" }}>
                    <View style={{ justifyContent: "center" }}>
                      <Icons
                        name="phone"
                        size={18}
                        color={Colors.divider}
                        style={{ marginHorizontal: 10 }}
                      />
                    </View>
                    <View style={{ justifyContent: "center", flex: 1 }}>
                      <TextInput
                        ref={phoneRef}
                        placeholder="Phone"
                        onChangeText={formik.handleChange("phone")}
                        onSubmitEditing={() =>
                          onSubmitEditing(TEXT_INPUT_PHONE)
                        }
                      />
                    </View>
                  </View>
                  <DTText
                    marginTop={4}
                    type="buttonRegular12"
                    color={Colors.error}
                  >
                    {formik.errors.phone}
                  </DTText>
                </View>

                <View style={styles.inputContainer}>
                  <View style={{ flexDirection: "row" }}>
                    <View style={{ justifyContent: "center" }}>
                      <Icons
                        name="lock"
                        size={18}
                        color={Colors.divider}
                        style={{ marginHorizontal: 10 }}
                      />
                    </View>
                    <View style={{ justifyContent: "center", flex: 1 }}>
                      <TextInput
                        ref={passwordRef}
                        placeholder="Password"
                        onChangeText={formik.handleChange("password")}
                        onSubmitEditing={() =>
                          onSubmitEditing(TEXT_INPUT_PASSWORD)
                        }
                      />
                    </View>
                  </View>
                  <DTText
                    marginTop={4}
                    type="buttonRegular12"
                    color={Colors.error}
                  >
                    {formik.errors.password}
                  </DTText>
                </View>

                <View style={styles.inputContainer}>
                  <View style={{ flexDirection: "row" }}>
                    <View style={{ justifyContent: "center" }}>
                      <Icons
                        name="lock"
                        size={18}
                        color={Colors.divider}
                        style={{ marginHorizontal: 10 }}
                      />
                    </View>
                    <View style={{ justifyContent: "center", flex: 1 }}>
                      <TextInput
                        ref={confirmPasswordRef}
                        placeholder="Confirm Password"
                        onChangeText={formik.handleChange("confirmPassword")}
                        onSubmitEditing={() =>
                          onSubmitEditing(TEXT_INPUT_CONFIRM_PASSWORD)
                        }
                      />
                    </View>
                  </View>
                  <DTText
                    marginTop={4}
                    type="buttonRegular12"
                    color={Colors.error}
                  >
                    {formik.errors.confirmPassword}
                  </DTText>
                </View>

                <TouchableWithoutFeedback onPress={formik.handleSubmit}>
                  <View
                    style={{
                      width: "100%",
                      height: 48,
                      backgroundColor: "#51cb96",
                      borderRadius: 5,
                      justifyContent: "center",
                      alignItems: "center",
                      marginTop: 8,
                    }}
                  >
                    <Text
                      style={{
                        color: Colors.white,
                        fontWeight: Fonts.fontWeight.medium,
                        fontSize: Fonts.fontSize.xMedium,
                      }}
                    >
                      Register
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
              </View>
            </View>
          </KeyboardAwareScrollView>
        </ImageBackground>
      </Container>
    </View>
  );
};

export default Register;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // flexDirection: "column",
    backgroundColor: Colors.divider,
  },
  image: {
    flex: 1,
  },
  text: {
    color: Colors.divider,
    fontSize: 30,
    fontWeight: "bold",
  },
  registerFormContainer: {
    alignSelf: "center",
    width: "90%",
    flex: 1,
    backgroundColor: Colors.white,
    borderRadius: 15,
    justifyContent: "space-evenly",
    paddingHorizontal: 15,
    marginVertical: 25,
    paddingVertical: 10,
  },
  shadowInput: {
    flexDirection: "row",
    shadowOffset: { width: 1, height: 1 },
    shadowColor: "black",
    backgroundColor: "white",
    shadowOpacity: 0.2,
    height: 45,
    elevation: 2,
  },
  inputContainer: {
    shadowOffset: { width: 1, height: 1 },
    shadowColor: "black",
    backgroundColor: "white",
    shadowOpacity: 0.2,
    height: 45,
    elevation: 2,
  },
});
