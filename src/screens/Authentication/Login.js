import React, { useState, useEffect, useRef } from "react";
import {
  View,
  Text,
  TouchableWithoutFeedback,
  TextInput,
  StyleSheet,
  ImageBackground,
  Image,
  ScrollView,
  KeyboardAvoidingView,
  Animated,
  TouchableHighlight,
} from "react-native";
import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";
import LoginActions from "../../redux/AuthRedux/actions";
import colors from "../../themes/Colors";
import { Fonts, Colors } from "../../themes";
import Icons from "react-native-vector-icons/FontAwesome";
import ShadowInput from "../../components/TextInput/ShadowInput";
import { NavigationUtils } from "../../navigation";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import DeviceInfo from "react-native-device-info";
import SiteActions from "../../redux/SiteRedux/actions";
import messaging from "@react-native-firebase/messaging";
import * as Yup from "yup";
import { useFormik } from "formik";
import { DTText } from "../../components";

const TEXT_INPUT_EMAIL = "TEXT_INPUT_EMAIL";
const TEXT_INPUT_PASSWORD = "TEXT_INPUT_PASSWORD";

const Login = () => {
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);
  const [SlideInBottom, setSlideInBottom] = useState(new Animated.Value(0));

  let emailRef = useRef(null);
  let passwordRef = useRef(null);

  const dispatch = useDispatch();

  useEffect(() => {
    Animated.timing(SlideInBottom, {
      toValue: 1,
      duration: 1000,
      useNativeDriver: true,
    }).start();
  }, []);

  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: Yup.object({
      email: Yup.string()
        .email("Email is invalid")
        .required("Email is required"),
      password: Yup.string()
        .min(6, "Password is at least 6 characters")
        .max(20, "Password is least than 20 characters")
        .required("Password is required"),
    }),
    onSubmit: (values) => {
      onLogin(values);
    },
  });

  const onSubmitEditing = (field) => {
    if (field === TEXT_INPUT_EMAIL) {
      passwordRef.current?.focus();
    }
    if (field === TEXT_INPUT_PASSWORD) {
      passwordRef.current?.blur();
      formik.handleSubmit();
    }
  };

  const onLogin = async ({ email, password }) => {
    const fcmToken = await messaging().getToken();
    const data = {
      // email: "tan1@gmail.com",
      // password: "123456",
      email,
      password,
      device_token: fcmToken,
    };
    await dispatch(LoginActions.login(data));
    await dispatch(SiteActions.getAllSite());
  };
  const pushRegister = () => {
    NavigationUtils.push({
      screen: "Register",
      title: "Register",
      isBack: true,
      isTopBarEnable: true,
      noBorder: true,
    });
  };

  return (
    <View style={styles.container}>
      <ImageBackground
        source={require("../../assets/img/intro3.jpg")}
        style={styles.image}
      >
        <KeyboardAwareScrollView
          enableResetScrollToCoords={false}
          showsVerticalScrollIndicator={false}
        >
          <View style={{ alignSelf: "center" }}>
            <Image
              source={require("../../assets/img/logo.jpg")}
              style={styles.logo}
            />
          </View>
          <View style={{ alignItems: "center", marginVertical: 35 }}>
            <Text
              style={{
                color: "white",
                fontWeight: Fonts.fontWeight.semibold,
                fontSize: Fonts.fontSize.title,
              }}
            >
              Discover new place
            </Text>

            <Text
              style={{
                color: "white",
                fontWeight: Fonts.fontWeight.semibold,
                fontSize: Fonts.fontSize.title,
              }}
            >
              you will love
            </Text>
          </View>
          <Animated.View
            style={{
              transform: [
                {
                  translateY: SlideInBottom.interpolate({
                    inputRange: [0, 1],
                    outputRange: [300, 0],
                  }),
                },
              ],
            }}
          >
            <View style={styles.loginFormContainer}>
              <View style={{ justifyContent: "space-around", height: 150 }}>
                <View
                  style={{
                    shadowOffset: { width: 1, height: 1 },
                    shadowColor: "black",
                    backgroundColor: "white",
                    shadowOpacity: 0.2,
                    height: 45,
                    elevation: 2,
                  }}
                >
                  <View style={{ flexDirection: "row" }}>
                    <View style={{ justifyContent: "center" }}>
                      <Icons
                        name="envelope"
                        size={18}
                        color={colors.divider}
                        style={{ marginHorizontal: 10 }}
                      />
                    </View>
                    <View style={{ justifyContent: "center", flex: 1 }}>
                      <TextInput
                        ref={emailRef}
                        placeholder="Email"
                        onChangeText={formik.handleChange("email")}
                        onSubmitEditing={() =>
                          onSubmitEditing(TEXT_INPUT_EMAIL)
                        }
                      />
                    </View>
                  </View>
                  <DTText
                    marginTop={4}
                    type="buttonRegular12"
                    color={Colors.error}
                  >
                    {formik.errors.email}
                  </DTText>
                </View>

                <View
                  style={{
                    shadowOffset: { width: 1, height: 1 },
                    shadowColor: "black",
                    backgroundColor: "white",
                    shadowOpacity: 0.2,
                    height: 45,
                    elevation: 2,
                  }}
                >
                  <View style={{ flexDirection: "row" }}>
                    <View style={{ justifyContent: "center" }}>
                      <Icons
                        name="lock"
                        size={18}
                        color={colors.divider}
                        style={{ marginHorizontal: 10 }}
                      />
                    </View>
                    <View style={{ justifyContent: "center", flex: 1 }}>
                      <TextInput
                        ref={passwordRef}
                        placeholder="Password"
                        onChangeText={formik.handleChange("password")}
                        onSubmitEditing={() =>
                          onSubmitEditing(TEXT_INPUT_PASSWORD)
                        }
                      />
                    </View>
                  </View>
                  <DTText
                    marginTop={4}
                    type="buttonRegular12"
                    color={Colors.error}
                  >
                    {formik.errors.password}
                  </DTText>
                </View>
              </View>

              <TouchableHighlight onPress={formik.handleSubmit}>
                <View
                  style={{
                    width: "100%",
                    height: 48,
                    backgroundColor: "#51cb96",
                    borderRadius: 5,
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Text
                    style={{
                      color: colors.white,
                      fontWeight: Fonts.fontWeight.medium,
                      fontSize: Fonts.fontSize.xMedium,
                    }}
                  >
                    Login
                  </Text>
                </View>
              </TouchableHighlight>
              <TouchableWithoutFeedback onPress={pushRegister}>
                <View
                  style={{
                    width: "100%",
                    backgroundColor: colors.white,
                    borderRadius: 5,
                    alignItems: "center",
                    borderColor: "#51cb96",
                    borderWidth: 1,
                    justifyContent: "center",
                    height: 48,
                  }}
                >
                  <Text
                    style={{
                      color: "#51cb96",
                      fontWeight: Fonts.fontWeight.regular,
                      fontSize: Fonts.fontSize.xMedium,
                    }}
                  >
                    Register
                  </Text>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </Animated.View>
        </KeyboardAwareScrollView>
      </ImageBackground>
    </View>
  );
};

export default Login;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: colors.divider,
  },
  image: {
    flex: 1,
  },
  text: {
    color: colors.divider,
    fontSize: 30,
    fontWeight: "bold",
  },
  logo: {
    width: 105,
    height: 105,
    borderRadius: 35,
    marginTop: 50,
  },
  loginFormContainer: {
    alignSelf: "center",
    width: "90%",
    height: 300,
    backgroundColor: colors.white,
    borderRadius: 15,
    justifyContent: "space-evenly",
    paddingHorizontal: 15,
  },
});
