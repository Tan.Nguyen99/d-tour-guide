import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  TouchableWithoutFeedback,
  Platform,
  Image,
  FlatList,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { NavigationUtils } from "../navigation";
import { Colors, Fonts, Metrics } from "../themes";
import { DTText } from "../components";
import { getTourByStatusApi, getHistoryApi } from "../api/tour";
import moment from "moment";

const History = () => {
  const TOUR_GUIDE_LEVEL = {
    1: { price: 5, name: "Fresher" },
    2: { price: 6, name: "Junior" },
    3: { price: 7, name: "Senior" },
  };
  const userData = useSelector((state) => state.login.user);
  const [historyData, setHistoryData] = useState([]);
  const onGetHistory = async () => {
    try {
      const response = await getHistoryApi({
        user_id: userData.id,
        type: "tourist",
      });
      console.log("response", response);
      setHistoryData(response.data);
    } catch (error) {
      console.log("error", error);
    }
  };
  useEffect(() => {
    onGetHistory();
  }, []);

  const renderItem = ({ item }) => {
    return (
      <View
        style={{
          borderRadius: 8,
          backgroundColor: Colors.divider,
          marginBottom: 8,
          width: Metrics.screenWidth - 40,
          alignSelf: "center",
        }}
      >
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            paddingHorizontal: 8,
          }}
        >
          <DTText>Tour Id: {item.id}</DTText>
          <DTText>{item.status_name}</DTText>
        </View>
        <View style={{ backgroundColor: "white", flexDirection: "row" }}>
          <Image
            source={{ uri: item.tourist_sites[0].image }}
            style={{ width: 100, height: 100 }}
          />
          <View>
            <DTText>{item.pickup_location}</DTText>
            <DTText>
              {moment(item.canceled_at).format("hh:mm DD-MM-YYYY")}
            </DTText>
            <DTText>{TOUR_GUIDE_LEVEL[item.tour_guide_level].name}</DTText>

            <DTText>
              $
              {item.tourist_sites.length *
                TOUR_GUIDE_LEVEL[item.tour_guide_level].price}
            </DTText>
          </View>
        </View>
      </View>
    );
  };

  return (
    <View style={{ paddingBottom: 70 }}>
      <View
        style={{
          alignItems: "center",
          height: 55,
          justifyContent: "center",
          borderBottomColor: Colors.neutralDarkBlack80,
          borderBottomWidth: 0.5,
          marginBottom: 12,
        }}
      >
        <DTText type="h4">History</DTText>
      </View>
      <FlatList
        data={historyData}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
};

export default History;
