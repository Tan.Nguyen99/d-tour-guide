import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  ImageBackground,
  TouchableWithoutFeedback,
  TextInput,
  Alert,
} from "react-native";
import { Container } from "../../components";
import Icon from "react-native-vector-icons/Ionicons";
import { Colors, Metrics, Fonts } from "../../themes";
import { ratingPLaceApi } from "../../api/site";
import { useSelector, useDispatch } from "react-redux";
import { NavigationUtils } from "../../navigation";
import SiteActions from "../../redux/SiteRedux/actions";
import TourActions from "../../redux/BookTourRedux/actions";
import _ from "lodash";

const FinishTour = (props) => {
  const [star, setStar] = useState([
    { id: 0, isSelected: false },
    { id: 1, isSelected: false },
    { id: 2, isSelected: false },
    { id: 3, isSelected: false },
    { id: 4, isSelected: false },
  ]);
  const [guestComment, setGuestComment] = useState([
    { id: 0, value: "Very good tour guide", isSelected: false },
    { id: 1, value: "Very good", isSelected: false },
    { id: 2, value: "Super good", isSelected: false },
    { id: 3, value: "Out standing", isSelected: false },
    { id: 4, value: "Perfect", isSelected: false },
  ]);

  const [comment, setComment] = useState("Beautiful Place");
  const dispatch = useDispatch();
  const userData = useSelector((state) => state.login.user);
  const tourId = useSelector((state) => state.tour.tourId);

  const onGetTour = async () => {
    await dispatch(TourActions.getTour(tourId));
  };
  useEffect(() => {
    onGetTour();
  }, []);

  const checkStar = (id) => () => {
    setStar(
      star.map((item) => {
        if (star[id].isSelected === false) {
          if (item.id <= id) {
            item.isSelected = true;
          }
        } else {
          if (item.id > id) {
            item.isSelected = false;
          }
        }

        return item;
      })
    );
  };

  useEffect(() => {
    if (props.isPLaceRating) {
      setGuestComment([
        { id: 0, value: "Beautiful place", isSelected: false },
        { id: 1, value: "Nice food", isSelected: false },
        { id: 2, value: "Amazing", isSelected: false },
        { id: 3, value: "Very interesting", isSelected: false },
        { id: 4, value: "Perfect", isSelected: false },
      ]);
      setStar(props.star);
    }
  }, []);

  const onRating = async () => {
    let rate = star.map((item) => {
      if (item.isSelected) {
        return item;
      } else {
        return null;
      }
    });
    // console.log("star", _.compact(rate));

    try {
      const commentData = guestComment.map((item) => {
        if (item.isSelected === true) {
          return item.value;
        }
        return null;
      });

      let data = {
        tourist_id: userData.id,
        tourist_site_id: props.isPLaceRating ? props.site.id : null,
        tour_guide_id: "",
        votable_type: props.isPLaceRating ? "tourist_site" : "tour_guide",
        content: comment + commentData.toString().replace(/,/g, ""),
        rating: _.compact(rate).length,
      };
      const response = await ratingPLaceApi(data);
      if (props.isPLaceRating) {
        await dispatch(SiteActions.getSiteById(props.site.id));
      }
      let tourData = {
        tourist_id: userData.id,
        tour_guide_id: null,
        promotion_code: null,
        tourist_site_id: null,
        pickup_location: "101B Ngo Quyen",
        start_time: "09:20:00",
        unit_price: 5,
        total_guests: 1,
        status: 0,
      };
      await dispatch(TourActions.createTour(tourData));
      Alert.alert("Success", "Thank you for using D Travel");
      NavigationUtils.startMainContent();
    } catch (error) {
      console.log("error", error);

      Alert.alert("Error", "Something wrong");
    }
  };

  const checkComment = (id) => async () => {
    setGuestComment(
      guestComment.map((item) => {
        if (item.id === id) {
          item.isSelected = !item.isSelected;
        }

        return item;
      })
    );
  };
  return (
    <Container haveTextInput style={{ flex: 1 }}>
      <View style={{ flex: 1, marginTop: 55 }}>
        {props.isPLaceRating ? (
          <ImageBackground
            source={{ uri: props.image }}
            style={{
              height: (Metrics.screenHeight * 2) / 5 - 100,
              width: Metrics.screenWidth - 30,
              alignSelf: "center",
              alignItems: "center",
              justifyContent: "flex-end",
              marginBottom: 12,
            }}
          >
            <View
              style={{
                flexDirection: "row",
                marginBottom: -12,
                backgroundColor: Colors.white,
                borderRadius: 20,
                borderColor: Colors.warning,
                borderWidth: 1,
                paddingHorizontal: 5,
              }}
            >
              {star.map((item) => {
                return (
                  <TouchableWithoutFeedback onPress={checkStar(item.id)}>
                    <View
                      style={{ paddingHorizontal: 6, paddingVertical: 5 }}
                      key={item.id}
                    >
                      <Icon
                        name={item.isSelected ? "ios-star" : "ios-star-outline"}
                        size={24}
                        color={Colors.warning}
                      />
                    </View>
                  </TouchableWithoutFeedback>
                );
              })}
            </View>
          </ImageBackground>
        ) : (
          <View
            style={{
              height: (Metrics.screenHeight * 2) / 5 - 100,
              width: Metrics.screenWidth - 30,
              alignSelf: "center",
              backgroundColor: Colors.primaryDark,
              alignItems: "center",
              justifyContent: "center",
              paddingVertical: 8,
            }}
          >
            <Image
              source={require("../../assets/img/tourguide.jpg")}
              style={{ width: 110, height: 110, borderRadius: 100 }}
            />
            <View style={{ flexDirection: "row", marginTop: 12 }}>
              {star.map((item) => {
                return (
                  <TouchableWithoutFeedback onPress={checkStar(item.id)}>
                    <Icon
                      name={item.isSelected ? "ios-star" : "ios-star-outline"}
                      size={32}
                      color={Colors.warning}
                      style={{ marginHorizontal: 6 }}
                    />
                  </TouchableWithoutFeedback>
                );
              })}
            </View>
          </View>
        )}
        <View
          style={{
            flexDirection: "row",
            flexWrap: "wrap",
            justifyContent: "center",
            paddingHorizontal: 15,
            marginVertical: 5,
          }}
        >
          {guestComment.map((item) => {
            return (
              <TouchableWithoutFeedback onPress={checkComment(item.id)}>
                <View
                  style={{
                    height: 35,
                    justifyContent: "center",
                    alignItems: "center",
                    borderWidth: 1,
                    borderColor: item.isSelected
                      ? Colors.primary
                      : Colors.darkGray,
                    borderRadius: 20,
                    marginHorizontal: 5,
                    marginVertical: 5,
                  }}
                >
                  <Text
                    style={{
                      paddingHorizontal: 10,
                      color: item.isSelected ? Colors.primary : Colors.darkGray,
                    }}
                  >
                    {item.value}
                  </Text>
                </View>
              </TouchableWithoutFeedback>
            );
          })}
        </View>
        <View style={{ justifyContent: "flex-start" }}>
          <TextInput
            multiline
            style={styles.textInput}
            placeholder={
              props.isPLaceRating
                ? "How do you feel about this place?"
                : "How do you feel about this tour?"
            }
            value={comment}
            onChangeText={(text) => {
              setComment(text);
            }}
          />
        </View>
        <View
          style={{
            justifyContent: "flex-end",
            alignItems: "center",
            height: (Metrics.screenHeight * 1) / 5 - 55,
          }}
        >
          <TouchableWithoutFeedback onPress={onRating}>
            <View
              style={{
                width: Metrics.screenWidth - 24,
                height: 48,
                backgroundColor: "#51cb96",
                borderRadius: 5,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Text
                style={{
                  color: Colors.white,
                  fontWeight: Fonts.fontWeight.medium,
                  fontSize: Fonts.fontSize.xMedium,
                }}
              >
                Rating
              </Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
    </Container>
  );
};

export default FinishTour;
const styles = StyleSheet.create({
  textInput: {
    borderWidth: 1,
    borderColor: Colors.divider,
    paddingHorizontal: 16,
    paddingTop: 16,
    textAlignVertical: "top",
    height: (Metrics.screenHeight * 2) / 5 - 100,
    marginHorizontal: 20,
  },
});
