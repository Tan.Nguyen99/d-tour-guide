import React, { useEffect } from "react";
import { View, StyleSheet, Dimensions } from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { values } from "lodash";
import { Text, EmptyView } from "../../components";
import { TabView, SceneMap, TabBar } from "react-native-tab-view";
import { Colors, Metrics } from "../../themes";
import { Navigation } from "react-native-navigation";
import { NavigationUtils } from "../../navigation";
import { Maps } from "../Maps";
import ProcessTour from "./ProcessTour";
import TourActions from "../../redux/BookTourRedux/actions";

const TrackingTour = (props) => {
  const dispatch = useDispatch();
  const listSites = useSelector((state) => state.tour.data.tourist_sites);
  const tourId = useSelector((state) => state.tour.tourId);
  const status = useSelector((state) => state.tour.status_name);

  Navigation.events().registerNavigationButtonPressedListener(
    ({ buttonId }) => {
      if (buttonId === "backToRoot") {
        Navigation.popToRoot(props.componentId);
      }
    }
  );

  const FirstRoute = () => {
    return <Maps coordinates={listSites} />;
  };
  const onMapPress = () => {};

  const onGetTour = async () => {
    await dispatch(TourActions.getTour(tourId));
  };

  const SecondRoute = () => <ProcessTour />;

  const renderTabBar = (props) => (
    <TabBar
      {...props}
      indicatorStyle={styles.tabBar}
      style={{ backgroundColor: Colors.white }}
      renderLabel={({ route, focused }) => (
        <Text type="bold16" color={Colors.blackText}>
          {route.title}
        </Text>
      )}
    />
  );

  const initialLayout = { width: Dimensions.get("window").width };

  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: "first", title: "Maps" },
    { key: "second", title: "Process" },
  ]);

  React.useEffect(() => {
    if (index === 1) {
      onGetTour();
    }
  }, [index]);

  const renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
  });

  if (status === "FINISHED") {
    return <EmptyView title={"Empty view"} />;
  }

  return (
    <View style={styles.container}>
      <View style={{ marginTop: 40, flex: 1 }}>
        <TabView
          navigationState={{ index, routes }}
          renderScene={renderScene}
          onIndexChange={setIndex}
          initialLayout={initialLayout}
          renderTabBar={renderTabBar}
          //   onSwipeEnd={getTour}
        />
      </View>
    </View>
  );
};

export default TrackingTour;
const styles = StyleSheet.create({
  container: { flex: 1 },

  tabBar: {
    backgroundColor: Colors.primary,
    height: 3,
    width: (Metrics.screenWidth - 80) / 2,
    marginLeft: 20,
  },
});
