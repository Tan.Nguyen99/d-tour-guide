import React from "react";
import {
  StyleSheet,
  SafeAreaView,
  FlatList,
  Text,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import moment from "moment";
import { Colors, Fonts, Metrics } from "../../themes";
import ProcessTime from "../../components/ProcessTime";
import TourActions from "../../redux/BookTourRedux/actions";
import { connect } from "react-redux";
import { Container, DTText } from "../../components";
import { NavigationUtils } from "../../navigation";

const PROCESS = [
  {
    id: 0,
    tag: "PENDING",
    timeTag: "pending_at",
    contentTransId: "Waiting for tour guide",
    icon: "ios-cart-outline",
  },
  {
    id: 1,
    tag: "IMPLEMENTING",
    timeTag: "implementing_at",
    contentTransId: "On going tour",
    icon: "ios-timer",
  },
  {
    id: 2,
    tag: "FINISHED",
    timeTag: "finished_at",
    contentTransId: "Finish tour",
    icon: "ios-walk",
  },
];

class ProcessTour extends React.PureComponent {
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     processData: null,
  //   };
  // }

  componentDidMount() {
    // this.props.getTour(this.props.tourId);
  }

  filterItem = (filterTag) => {
    const { data } = this.props;
    const processData = PROCESS;
    const filtered = processData.filter((item) => item.tag === filterTag);
    return filtered[0];
  };

  // componentDidUpdate(prevProps) {
  //   // Typical usage (don't forget to compare props):
  //   if (this.props.tourData !== prevProps.tourData) {
  //   }
  // }

  renderRow = ({ item, index }) => {
    const { tourData } = this.props;

    const markedData = this.filterItem(this.props.tourData.status_name);

    const processData = PROCESS;

    if (tourData.status_name === "CANCEL") {
      const CANCELLED_PROCESS = PROCESS.map((element) => {
        if (element.id === 2) {
          return Object.assign(element, {
            id: 2,
            tag: "CANCEL",
            timeTag: "canceled_at",
            contentTransId: "Tour cancel",
            icon: "ios-close",
          });
        }
        return element;
      });

      return (
        <ProcessTime
          icon="ios-close"
          iconColor={Colors.error}
          data={CANCELLED_PROCESS}
          lineColor={Colors.error}
          indexItem={index}
          title={item.contentTransId}
          time={
            tourData[item.timeTag]
              ? moment(tourData[item.timeTag]).fromNow()
              : "waitingTime"
          }
          timeColor={Colors.error}
        />
      );
    } else if (markedData) {
      return (
        <ProcessTime
          icon={
            index <= markedData.id ? "ios-checkmark-circle-outline" : item.icon
          }
          iconColor={
            index <= markedData.id ? Colors.primary : Colors.neutralGrey
          }
          title={item.contentTransId}
          titleColor={
            index <= markedData.id ? Colors.neutralBlack : Colors.neutralGrey
          }
          data={processData}
          lineColor={
            index <= markedData.id ? Colors.primary : Colors.neutralGrey
          }
          indexItem={index}
          time={
            tourData[item.timeTag]
              ? moment(tourData[item.timeTag]).fromNow()
              : "waitingTime"
          }
          timeColor={
            index <= markedData.id ? Colors.primary : Colors.neutralGrey
          }
        />
      );
    }
  };
  onCancel = () => {
    NavigationUtils.push({
      screen: "CancelTour",
      isBack: true,
      isTopBarEnable: true,
      title: "Cancel Tour",
      color: "white",
      leftButtonsColor: "black",
    });
  };

  render() {
    const { data, loading, tourData } = this.props;

    const processData = PROCESS;
    return (
      <Container scrollEnabled loading={loading}>
        <FlatList
          data={processData}
          renderItem={this.renderRow}
          keyExtractor={(item, index) => index.toString()}
          contentContainerStyle={styles.container}
          extraData={data}
        />
        <View style={{ height: 450 }}>
          <View style={{ paddingHorizontal: 20 }}>
            <View
              style={{
                width: Metrics.screenWidth - 40,
                backgroundColor: Colors.primary,
                padding: 5,
              }}
            >
              <DTText type="bold14" color={"white"}>
                CUSTOMER INFO
              </DTText>
            </View>
            <View style={styles.infoRow}>
              <DTText
                type="regular14"
                color={Colors.neutralBlack}
                style={{ flex: 3 }}
              >
                Full name:{" "}
              </DTText>
              <DTText type="regular14" style={{ flex: 5 }}>
                {this.props.userData.full_name}
              </DTText>
            </View>

            <View style={styles.infoRow}>
              <DTText
                type="regular14"
                color={Colors.neutralBlack}
                style={{ flex: 3 }}
              >
                Email:{" "}
              </DTText>
              <DTText type="regular14" style={{ flex: 5 }}>
                {this.props.userData.email}
              </DTText>
            </View>

            <View style={styles.infoRow}>
              <DTText
                type="regular14"
                color={Colors.neutralBlack}
                style={{ flex: 3 }}
              >
                Phone number:{" "}
              </DTText>
              <DTText type="regular14" style={{ flex: 5 }}>
                {this.props.userData.phone_number}
              </DTText>
            </View>

            <View
              style={{
                width: Metrics.screenWidth - 40,
                backgroundColor: Colors.primary,
                padding: 5,
              }}
            >
              <DTText type="bold14" color={"white"}>
                TOUR INFO
              </DTText>
            </View>

            <View style={styles.infoRow}>
              <DTText
                type="regular14"
                color={Colors.neutralBlack}
                style={{ flex: 3 }}
              >
                Pick up location:{" "}
              </DTText>
              <DTText type="regular14" style={{ flex: 5 }}>
                {tourData.pickup_location}
              </DTText>
            </View>

            <View style={styles.infoRow}>
              <DTText
                type="regular14"
                color={Colors.neutralBlack}
                style={{ flex: 3 }}
              >
                Total tourist:{" "}
              </DTText>
              <DTText type="regular14" style={{ flex: 5 }}>
                {tourData.total_guests}
              </DTText>
            </View>

            <View style={styles.infoRow}>
              <DTText
                type="regular14"
                color={Colors.neutralBlack}
                style={{ flex: 3 }}
              >
                Number of place:{" "}
              </DTText>
              <DTText type="regular14" style={{ flex: 5 }}>
                {tourData.tourist_sites.length}
              </DTText>
            </View>

            <View style={styles.infoRow}>
              <DTText
                type="regular14"
                color={Colors.neutralBlack}
                style={{ flex: 3 }}
              >
                Tour duration:{" "}
              </DTText>
              <DTText type="regular14" style={{ flex: 5 }}>
                {tourData.start_time}
              </DTText>
            </View>

            <View style={styles.infoRow}>
              <DTText
                type="regular14"
                color={Colors.neutralBlack}
                style={{ flex: 3 }}
              >
                Total price:{" "}
              </DTText>
              <DTText type="regular14" style={{ flex: 5 }}>
                ${tourData.unit_price}
              </DTText>
            </View>
          </View>
        </View>
        <TouchableWithoutFeedback onPress={this.onCancel}>
          <View
            style={{
              width: Metrics.screenWidth - 24,
              height: 48,
              backgroundColor: Colors.white,
              borderRadius: 5,
              justifyContent: "center",
              alignItems: "center",
              alignSelf: "center",
              marginVertical: 20,
              borderColor: Colors.divider,
              borderWidth: 2,
            }}
          >
            <Text
              style={{
                color: Colors.divider,
                fontWeight: Fonts.fontWeight.medium,
                fontSize: Fonts.fontSize.xMedium,
              }}
            >
              Cancel tour
            </Text>
          </View>
        </TouchableWithoutFeedback>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    paddingVertical: 44,
  },
  flexContainer: {
    flex: 1,
  },
  infoRow: {
    width: Metrics.screenWidth - 40,
    flexDirection: "row",
    alignItems: "flex-end",
    height: 40,
    alignItems: "center",
    borderBottomColor: Colors.divider,
    paddingHorizontal: 8,
    borderBottomWidth: 1,
  },
});

const mapStateToProps = (state, props) => ({
  tourData: state.tour.data,
  tourId: state.tour.tourId,
  loading: state.tour.getTourLoading,
  userData: state.login.data,
});

const mapDispatchToProps = (dispatch) => ({
  getTour: (data) => dispatch(TourActions.getTour(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProcessTour);
