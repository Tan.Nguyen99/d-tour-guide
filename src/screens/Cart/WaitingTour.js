import React, { useState, useEffect } from "react";

import { View, Text, StyleSheet, TouchableWithoutFeedback } from "react-native";
import LottieView from "lottie-react-native";
import { Container, DTText } from "../../components";
import { useSelector, useDispatch } from "react-redux";
import { Metrics, Colors, Fonts } from "../../themes";
import { NavigationUtils } from "../../navigation";
import TourActions from "../../redux/BookTourRedux/actions";

const WaitingTour = () => {
  const dispatch = useDispatch();
  const userData = useSelector((state) => state.login.data);
  const tourData = useSelector((state) => state.tour.data);
  const tourId = useSelector((state) => state.tour.tourId);
  const bookTourData = useSelector((state) => state.app.data);

  const onGetTour = async () => {
    await dispatch(TourActions.getTour(tourId));
  };
  const onBookTourAgain = async () => {
    await dispatch(TourActions.updateTour(bookTourData));
    Navigation.popToRoot(props.componentId);
  };

  useEffect(() => {
    onGetTour();
  }, []);
  const onCancel = () => {
    NavigationUtils.push({
      screen: "CancelTour",
      isBack: true,
      isTopBarEnable: true,
      title: "Cancel Tour",
      color: "white",
      leftButtonsColor: "black",
    });
  };

  return (
    <Container scrollEnabled>
      <View
        style={{
          flex: 1,
          justifyContent: "flex-start",
          alignItems: "center",
          marginTop: 12,
        }}
      >
        <LottieView
          source={require("../../assets/lottieFile/lf30_editor_BLlKc0.json")}
          autoPlay
          style={{ width: 240, height: 240 }}
          loop
        />
        <DTText
          type="h2"
          marginTop={-20}
          marginBottom={12}
          color={Colors.neutralBlack}
          center
        >
          We are finding tour guide for you
        </DTText>
      </View>
      <View style={{ flex: 2 }}>
        <View style={{ paddingHorizontal: 20 }}>
          <View
            style={{
              width: Metrics.screenWidth - 40,
              backgroundColor: Colors.primary,
              padding: 5,
            }}
          >
            <DTText type="bold14" color={"white"}>
              CUSTOMER INFO
            </DTText>
          </View>
          <View style={styles.infoRow}>
            <DTText
              type="regular14"
              color={Colors.neutralBlack}
              style={{ flex: 3 }}
            >
              Full name:{" "}
            </DTText>
            <DTText type="regular14" style={{ flex: 5 }}>
              {userData.full_name}
            </DTText>
          </View>

          <View style={styles.infoRow}>
            <DTText
              type="regular14"
              color={Colors.neutralBlack}
              style={{ flex: 3 }}
            >
              Email:{" "}
            </DTText>
            <DTText type="regular14" style={{ flex: 5 }}>
              {userData.email}
            </DTText>
          </View>

          <View style={styles.infoRow}>
            <DTText
              type="regular14"
              color={Colors.neutralBlack}
              style={{ flex: 3 }}
            >
              Phone number:{" "}
            </DTText>
            <DTText type="regular14" style={{ flex: 5 }}>
              {userData.phone_number}
            </DTText>
          </View>

          <View
            style={{
              width: Metrics.screenWidth - 40,
              backgroundColor: Colors.primary,
              padding: 5,
            }}
          >
            <DTText type="bold14" color={"white"}>
              TOUR INFO
            </DTText>
          </View>

          <View style={styles.infoRow}>
            <DTText
              type="regular14"
              color={Colors.neutralBlack}
              style={{ flex: 3 }}
            >
              Pick up location:{" "}
            </DTText>
            <DTText type="regular14" style={{ flex: 5 }}>
              {tourData.pickup_location}
            </DTText>
          </View>

          <View style={styles.infoRow}>
            <DTText
              type="regular14"
              color={Colors.neutralBlack}
              style={{ flex: 3 }}
            >
              Total tourist:{" "}
            </DTText>
            <DTText type="regular14" style={{ flex: 5 }}>
              {tourData.total_guests}
            </DTText>
          </View>

          <View style={styles.infoRow}>
            <DTText
              type="regular14"
              color={Colors.neutralBlack}
              style={{ flex: 3 }}
            >
              Number of place:{" "}
            </DTText>
            <DTText type="regular14" style={{ flex: 5 }}>
              {tourData.tourist_sites.length}
            </DTText>
          </View>

          <View style={styles.infoRow}>
            <DTText
              type="regular14"
              color={Colors.neutralBlack}
              style={{ flex: 3 }}
            >
              Tour duration:{" "}
            </DTText>
            <DTText type="regular14" style={{ flex: 5 }}>
              {tourData.start_time}
            </DTText>
          </View>

          <View style={styles.infoRow}>
            <DTText
              type="regular14"
              color={Colors.neutralBlack}
              style={{ flex: 3 }}
            >
              Total price:{" "}
            </DTText>
            <DTText type="regular14" style={{ flex: 5 }}>
              ${tourData.unit_price}
            </DTText>
          </View>
        </View>
      </View>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          paddingHorizontal: 20,
        }}
      >
        <TouchableWithoutFeedback onPress={onBookTourAgain}>
          <View
            style={{
              width: (Metrics.screenWidth - 55) / 2,
              height: 48,
              backgroundColor: Colors.primary,
              borderRadius: 5,
              justifyContent: "center",
              alignItems: "center",
              alignSelf: "center",
              marginVertical: 20,
              borderColor: Colors.primary,
              borderWidth: 2,
            }}
          >
            <Text
              style={{
                color: Colors.white,
                fontWeight: Fonts.fontWeight.medium,
                fontSize: Fonts.fontSize.xMedium,
              }}
            >
              Book tour again
            </Text>
          </View>
        </TouchableWithoutFeedback>

        <TouchableWithoutFeedback onPress={onCancel}>
          <View
            style={{
              width: (Metrics.screenWidth - 55) / 2,
              height: 48,
              backgroundColor: Colors.white,
              borderRadius: 5,
              justifyContent: "center",
              alignItems: "center",
              alignSelf: "center",
              marginVertical: 20,
              borderColor: Colors.divider,
              borderWidth: 2,
            }}
          >
            <Text
              style={{
                color: Colors.divider,
                fontWeight: Fonts.fontWeight.medium,
                fontSize: Fonts.fontSize.xMedium,
              }}
            >
              Cancel tour
            </Text>
          </View>
        </TouchableWithoutFeedback>
      </View>
    </Container>
  );
};

export default WaitingTour;
const styles = StyleSheet.create({
  infoRow: {
    width: Metrics.screenWidth - 40,
    flexDirection: "row",
    alignItems: "flex-end",
    height: 40,
    alignItems: "center",
    borderBottomColor: Colors.divider,
    paddingHorizontal: 8,
    borderBottomWidth: 1,
  },
});
