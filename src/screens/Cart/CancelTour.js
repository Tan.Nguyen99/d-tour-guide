import React, { useState } from "react";
import { View, Text, FlatList, StyleSheet, Alert } from "react-native";
import TourActions from "../../redux/BookTourRedux/actions";
import { Container, DTText } from "../../components";
import { useSelector, useDispatch } from "react-redux";
import { Colors, Metrics, Fonts } from "../../themes";
import {
  TouchableWithoutFeedback,
  TextInput,
} from "react-native-gesture-handler";
import { NavigationUtils } from "../../navigation";
import { Navigation } from "react-native-navigation";

const CancelTour = () => {
  const dispatch = useDispatch();
  const tourId = useSelector((state) => state.tour.tourId);
  const updateTourLoading = useSelector(
    (state) => state.tour.updateTourLoading
  );
  const [reasons, setReason] = useState([
    { id: 0, reason: "I want to change the place in tour", isSelect: false },
    { id: 1, reason: "I did not see the tour guide", isSelect: false },
    { id: 2, reason: "I want to change the start time", isSelect: false },
    { id: 3, reason: "I can not go to the pick up location", isSelect: false },
    { id: 4, reason: "I want to change tour guide", isSelect: false },
    { id: 6, reason: "Another reason", isSelect: false },
  ]);

  const [otherReason, setOtherReason] = useState(null);

  const onCancel = async () => {
    try {
      if (otherReason === null) {
        Alert.alert("Warning", "Please choose your reason!");
      } else {
        let data = {
          data: {
            promotion_id: null,
            pickup_location: "Google Building 43",
            pickup_latitude: null,
            pickup_longitude: null,
            start_time: null,
            end_time: null,
            unit_price: null,
            total_guests: null,
            image: null,
            canceled_by: "tourist",
            canceled_reason: otherReason,
            tour_guide_level: 1,
            status: 3,
          },
          id: tourId,
        };
        await dispatch(TourActions.updateTour(data));
        Alert.alert("Success", "Cancel Tour success");
        NavigationUtils.startMainContent();
      }
    } catch (error) {}
  };

  const onSelectReason = (id) => () => {
    setReason(
      reasons.map((item) => {
        if (item.id === id) {
          item.isSelect = true;
          setOtherReason(item.reason);
        } else {
          item.isSelect = false;
        }
        return item;
      })
    );
  };

  const renderItem = ({ item }) => {
    let items = null;
    if (item.id === 6 && item.isSelect === true) {
      items = [
        <TouchableWithoutFeedback onPress={onSelectReason(item.id)}>
          <View
            style={{
              height: 52,
              justifyContent: "center",
              paddingLeft: 12,
              borderRadius: 10,
              marginVertical: 6,
              borderWidth: 1,
              borderColor: item.isSelect
                ? Colors.primaryDark
                : Colors.neutralDarkBlack80,
              backgroundColor: item.isSelect ? Colors.primary : Colors.white,
            }}
          >
            <DTText
              color={item.isSelect ? Colors.white : Colors.neutralDarkBlack80}
            >
              {item.reason}
            </DTText>
          </View>
        </TouchableWithoutFeedback>,
        <View style={{ height: 120, marginTop: 6 }}>
          <TextInput
            multiline
            style={styles.textInput}
            placeholder={"Please enter your reason"}
            onChangeText={(text) => {
              setOtherReason(text);
            }}
          />
        </View>,
      ];
    } else {
      items = (
        <TouchableWithoutFeedback onPress={onSelectReason(item.id)}>
          <View
            style={{
              height: 52,
              justifyContent: "center",
              paddingLeft: 12,
              borderRadius: 10,
              marginVertical: 6,
              borderWidth: 1,
              borderColor: item.isSelect
                ? Colors.primaryDark
                : Colors.neutralDarkBlack80,
              backgroundColor: item.isSelect ? Colors.primary : Colors.white,
            }}
          >
            <DTText
              color={item.isSelect ? Colors.white : Colors.neutralDarkBlack80}
            >
              {item.reason}
            </DTText>
          </View>
        </TouchableWithoutFeedback>
      );
    }
    return items;
  };
  return (
    <Container scrollEnabled>
      <View
        style={{
          height: reasons[5].isSelect === true ? 600 : 450,
        }}
      >
        <FlatList
          showsHorizontalScrollIndicator={false}
          data={reasons}
          contentContainerStyle={{
            paddingHorizontal: 20,
            marginTop: 55,
          }}
          renderItem={renderItem}
          keyExtractor={(item) => item.id}
        />
      </View>
      <TouchableWithoutFeedback onPress={onCancel}>
        <View
          style={{
            width: Metrics.screenWidth - 24,
            height: 48,
            backgroundColor: Colors.white,
            borderRadius: 5,
            justifyContent: "center",
            alignItems: "center",
            alignSelf: "center",
            marginVertical: 20,
            borderColor: Colors.divider,
            borderWidth: 2,
          }}
        >
          <Text
            style={{
              color: Colors.divider,
              fontWeight: Fonts.fontWeight.medium,
              fontSize: Fonts.fontSize.xMedium,
            }}
          >
            Cancel tour
          </Text>
        </View>
      </TouchableWithoutFeedback>
    </Container>
  );
};

export default CancelTour;
const styles = StyleSheet.create({
  textInput: {
    borderWidth: 1,
    borderColor: Colors.divider,
    paddingHorizontal: 16,
    paddingTop: 16,
    textAlignVertical: "top",
    height: 120,
    // marginHorizontal: 20,
    // backgroundColor: "green",
  },
});
