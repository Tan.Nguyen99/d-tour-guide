import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Image,
  TouchableWithoutFeedback,
  FlatList,
  StyleSheet,
  Dimensions,
  ScrollView,
  TextInput,
  Alert,
} from "react-native";
import { Container, EmptyView, DTText } from "../../components";
import metrics from "../../themes/Metrics";
import Icons from "react-native-vector-icons/FontAwesome";
import Icon from "react-native-vector-icons/Ionicons";
import { Colors, Fonts, Metrics } from "../../themes";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import MapView, {
  PROVIDER_GOOGLE,
  Marker,
  Callout,
  Polygon,
  Polyline,
} from "react-native-maps";
import Geolocation from "@react-native-community/geolocation";
import RNAndroidLocationEnabler from "react-native-android-location-enabler";
import RNGooglePlaces from "react-native-google-places";
import { useSelector, useDispatch } from "react-redux";
import { NavigationUtils } from "../../navigation";
import moment from "moment";
import TourActions from "../../redux/BookTourRedux/actions";
import { deleteSiteInTourApi } from "../../api/tour";
import MultiSlider from "@ptomasroos/react-native-multi-slider";
import { fcmService } from "../../utils/FCMService";
import { localNotificationService } from "../../utils/LocalNotificationService";
import AppActions from "../../redux/AppRedux/actions";

const Cart = () => {
  const SITE = [];
  const dispatch = useDispatch();
  const currentLocation = useSelector((state) => state.app.currentLocation);
  const tourDataStore = useSelector((state) => state.tour);
  const tourId = useSelector((state) => state.tour.tourId);
  const user = useSelector((state) => state.login.user);
  const promotionData = useSelector((state) => state.tour.promotion);

  const loading = useSelector((state) => state.tour.getTourLoading);
  const updateTourLoading = useSelector(
    (state) => state.tour.updateTourLoading
  );

  const [currentLocationData, setCurrentLocationData] = useState({});
  const [startTime, setStartTime] = useState(null);
  const [star, setStar] = useState([
    { id: 1 },
    { id: 2 },
    { id: 3 },
    { id: 4 },
    { id: 5 },
  ]);
  const [numberGuest, setNumberGuest] = React.useState([1]);
  const [tourGuideLevel, setTourGuideLevel] = React.useState([
    { id: 1, level: "Fresher", isSelect: true },
    { id: 2, level: "Junior", isSelect: false },
    { id: 3, level: "Senior", isSelect: false },
  ]);
  const [level, setLevel] = useState(1);

  const [startDate, setStartDate] = useState();
  const [currentDate, setCurrentDate] = useState(new Date(moment()));
  const [promotionCode, setPromotionCode] = useState("");

  useEffect(() => {
    getTour();
    RNGooglePlaces.getCurrentPlace(["location", "name", "address"])
      .then((results) => {
        console.log(results[0]);
        setCurrentLocationData(results[0]);
      })
      .catch((error) => console.log(error.message));
  }, []);

  const bookTour = async () => {
    let data = {
      data: {
        tourist_id: user.id,
        tour_guide_id: null,
        promotion_id: promotionData === null ? null : promotionData.data.id,
        pickup_location: currentLocationData.name,
        pickup_latitude: currentLocationData.location.latitude,
        pickup_longitude: currentLocationData.location.longitude,
        start_time: moment(startTime).format("LTS"),
        end_time: "00:00:00",
        unit_price: 5,
        total_guests: numberGuest[0],
        image: "",
        tour_guide_level: level,
        status: 1,
      },

      id: tourId,
    };
    console.log("data", data);

    if (startTime == null) {
      Alert.alert("Warning", "Please pick the start time");
    } else {
      await dispatch(TourActions.updateTour(data));
      await dispatch(AppActions.setBookTourData(data));
      NavigationUtils.pop();
    }
  };

  const checkPromotion = async () => {
    let data = {
      promotion_code: promotionCode,
    };
    const response = await dispatch(TourActions.checkPromotion(data));
  };

  const getTour = async () => {
    if (tourId) {
      await dispatch(TourActions.getTour(tourId));
    }
  };
  const showDatePickerOverlay = (type) => {
    NavigationUtils.showOverlay({
      screen: "TourDatePicker",
      interceptTouchOutside: false,
      passProps: {
        startDate: currentDate,
        onReturnDate: (date) => {
          setStartTime(date);
        },
      },
    });
  };

  const deleteSite = async (id) => {
    const response = await deleteSiteInTourApi(id);
  };

  const onMap = () => {
    NavigationUtils.push({
      screen: "CartMap",
      passProps: { currentLocation },
      isBack: true,
      isTopBarEnable: true,
      noBorder: true,
      isDrawBehind: true,
      leftButtonsColor: Colors.black,
    });
  };

  const onSearchLocation = () => {
    RNGooglePlaces.openAutocompleteModal(
      {
        initialQuery: "",
        latitude: currentLocation.latitude,
        longitude: currentLocation.longitude,
        radius: 1,
        country: "VN",
        types: ["establishment", "address"],
      },
      [
        "placeID",
        "location",
        "name",
        "address",
        "types",
        "openingHours",
        "plusCode",
        "rating",
        "userRatingsTotal",
        "viewport",
      ]
    )
      .then((place) => {
        console.log(place);
        setCurrentLocationData(place);
      })
      .catch((error) => console.log(error.message));
  };
  const onBack = () => {
    NavigationUtils.pop();
  };

  const onCheckLevel = (id) => () => {
    setTourGuideLevel(
      tourGuideLevel.map((item) => {
        if (item.id === id) {
          item.isSelect = true;
          setLevel(item.id);
        } else {
          item.isSelect = false;
        }
        return item;
      })
    );
  };

  const renderItem = ({ item }) => {
    return (
      <View
        style={{
          width: metrics.screenWidth - 40,
          height: metrics.screenHeight / 6,
          borderRadius: 12,
          flexDirection: "row",
          borderWidth: 1,
          borderColor: Colors.divider,
          backgroundColor: "white",
          alignSelf: "center",
          marginTop: 10,
        }}
      >
        <Image
          source={{ uri: item.image }}
          style={{
            width: metrics.screenHeight / 6,
            height: metrics.screenHeight / 6,
            borderRadius: 12,
            alignSelf: "center",
          }}
        />
        <View style={{ flex: 1, padding: 10 }}>
          <View style={{ flexDirection: "row" }}>
            {star.map((item) => {
              return (
                <View
                  style={{
                    width: 20,
                    height: 20,
                    backgroundColor: Colors.warning,
                    marginRight: 5,
                    borderRadius: 20,
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                  key={item.id + "1"}
                >
                  <Icon name="ios-star" color={Colors.white} />
                </View>
              );
            })}
          </View>
          <View style={{ marginTop: 4 }}>
            <DTText type="displayTitleRegular16" color={Colors.primary}>
              {item.name}
            </DTText>
            <View
              style={{ flexDirection: "row", paddingRight: 10, marginTop: 4 }}
            >
              <Icon
                name="ios-compass"
                size={20}
                color={Colors.primary}
                style={{ marginRight: 4 }}
              />
              <DTText numberOfLines={2} ellipsizeMode="tail">
                {item.address}
              </DTText>
            </View>
          </View>

          <View
            style={{
              position: "absolute",
              right: 0,
            }}
          >
            <TouchableWithoutFeedback
              onPress={() => {
                deleteSite(item.id);
              }}
            >
              <View
                style={{
                  backgroundColor: Colors.error,
                  width: 24,
                  height: 24,
                  alignItems: "center",
                  justifyContent: "center",
                  borderRadius: 12,
                }}
              >
                <Icon name="md-close" size={16} color="white" />
              </View>
            </TouchableWithoutFeedback>
          </View>
        </View>
      </View>
    );
  };

  if (
    tourDataStore.data === null ||
    tourDataStore.data.tourist_sites.length === 0
  ) {
    return (
      <EmptyView
        title={"Don't have any place in tour"}
        buttonTitle={"See some places"}
        onPress={onBack}
      />
    );
  }
  return (
    <Container
      loading={loading}
      style={{ alignItems: "center", flex: 1, marginTop: 55 }}
    >
      <View
        style={{
          width: Metrics.screenWidth - 24,
          flex: 1,
        }}
      >
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={{}}>
            {/* <DateTimePickerModal isVisible={true} mode="date" /> */}
            <View style={{ backgroundColor: Colors.milk }}>
              <DTText type="buttonMedium14" marginVertical={10}>
                Pick up location
              </DTText>
              <View
                style={{
                  flexDirection: "row",
                  backgroundColor: Colors.white,
                  borderTopColor: Colors.primary,
                  borderTopWidth: 4,
                }}
              >
                <TouchableWithoutFeedback onPress={onMap}>
                  <MapView
                    initialRegion={currentLocation}
                    style={{
                      width: ((Metrics.screenWidth - 24) / 5) * 2,
                      height: 135,
                      marginRight: 5,
                    }}
                  >
                    <Marker
                      coordinate={{
                        latitude: currentLocation.latitude,
                        longitude: currentLocation.longitude,
                      }}
                    ></Marker>
                  </MapView>
                </TouchableWithoutFeedback>
                <View
                  style={{
                    width: ((Metrics.screenWidth - 24) / 5) * 3,
                    height: 135,
                    paddingHorizontal: 5,
                  }}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      alignContent: "center",
                      marginVertical: 10,
                    }}
                  >
                    <Icon
                      name="ios-compass"
                      size={24}
                      color={Colors.primary}
                      style={{ flex: 1 }}
                    />
                    <DTText type="subTitleRegular10" style={{ flex: 5 }}>
                      {currentLocationData.address}
                    </DTText>
                  </View>
                  <TouchableWithoutFeedback onPress={onSearchLocation}>
                    <View
                      style={{
                        flex: 1,
                        justifyContent: "flex-end",
                      }}
                    >
                      <View
                        style={{
                          flexDirection: "row",
                          alignItems: "center",
                          justifyContent: "center",
                          backgroundColor: Colors.primary,
                          borderRadius: 4,
                        }}
                      >
                        <DTText
                          type="subTitleRegular10"
                          color={Colors.white}
                          marginVertical={12}
                        >
                          Change pick up location
                        </DTText>
                      </View>
                    </View>
                  </TouchableWithoutFeedback>
                </View>
              </View>
            </View>
          </View>
          <View style={{ backgroundColor: Colors.milk, marginTop: 10 }}>
            <DTText type="buttonMedium14" marginVertical={10}>
              Tour Information
            </DTText>
            <View
              style={{
                backgroundColor: Colors.white,
                borderTopColor: Colors.primary,
                borderTopWidth: 4,
                height:
                  (metrics.screenHeight / 6 + 12) *
                  tourDataStore.data.tourist_sites.length,
              }}
            >
              <FlatList
                showsVerticalScrollIndicator={false}
                nestedScrollEnabled
                data={tourDataStore.data.tourist_sites}
                renderItem={renderItem}
                keyExtractor={(item) => item.id}
              />
            </View>
          </View>
          <View style={{ flexDirection: "row", marginTop: 20 }}>
            <View>
              <Text>Start time</Text>
              <TouchableWithoutFeedback onPress={showDatePickerOverlay}>
                <View
                  style={{
                    height: 48,
                    width: Metrics.screenWidth / 3 + 10,
                    borderColor: Colors.primary,
                    borderWidth: 1,
                    backgroundColor: Colors.white,
                    justifyContent: "space-between",
                    paddingHorizontal: 10,
                    flexDirection: "row",
                    alignItems: "center",
                    marginTop: 10,
                  }}
                >
                  <Icon name="ios-calendar" size={24} color={Colors.primary} />
                  <Text>
                    {startTime === null
                      ? "Start time"
                      : moment(startTime).format("LT")}
                  </Text>
                </View>
              </TouchableWithoutFeedback>
            </View>

            <View style={{ flex: 1, paddingLeft: 25 }}>
              <View style={{ flexDirection: "row" }}>
                <Text>Number of people:</Text>
                <Text
                  style={{ marginLeft: 4, color: Colors.primary, fontSize: 16 }}
                >
                  {numberGuest[0]}
                </Text>
              </View>

              <MultiSlider
                values={[1]}
                min={1}
                max={5}
                sliderLength={Dimensions.get("window").width / 2 - 6 * 2}
                onValuesChange={(values) => {
                  setNumberGuest(values);
                }}
                selectedStyle={{ backgroundColor: Colors.primary }}
                containerStyle={{ marginTop: 10 }}
                markerStyle={{ backgroundColor: Colors.primaryDark }}
                // onValuesChangeStart={sliderOneValuesChangeStart}
                // onValuesChange={sliderOneValuesChange}
                // onValuesChangeFinish={sliderOneValuesChangeFinish}
              />
            </View>
          </View>

          <View style={{ marginTop: 12 }}>
            <DTText>Level of tour guide</DTText>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-around",
                marginVertical: 12,
              }}
            >
              {tourGuideLevel.map((item) => {
                return (
                  <TouchableWithoutFeedback
                    onPress={onCheckLevel(item.id)}
                    key={item.id}
                  >
                    <View
                      style={{
                        backgroundColor: item.isSelect
                          ? Colors.primary
                          : "white",
                        width: (Metrics.screenWidth - 70) / 3,
                        height: 40,
                        borderRadius: 8,
                        justifyContent: "center",
                        alignItems: "center",
                        borderColor: Colors.primary,
                        borderWidth: 1,
                      }}
                    >
                      <Text
                        style={{
                          color: item.isSelect ? Colors.white : Colors.primary,
                        }}
                      >
                        {item.level}
                      </Text>
                    </View>
                  </TouchableWithoutFeedback>
                );
              })}
            </View>
          </View>

          <View
            style={{
              marginVertical: 15,
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
              borderTopColor: Colors.divider,
              borderTopWidth: 1,
            }}
          >
            <TextInput
              style={{
                borderColor: Colors.primary,
                borderWidth: 1,
                height: 40,
                flex: 2,
                paddingHorizontal: 10,
              }}
              placeholder="Promotion Code"
              onChangeText={(text) => {
                setPromotionCode(text);
              }}
            />
            <TouchableWithoutFeedback onPress={checkPromotion}>
              <View
                style={{
                  flex: 1,
                  height: 40,
                  backgroundColor: Colors.primary,
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Text style={{ color: Colors.white }}>Use Promotion</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>
          <View
            style={{
              borderTopColor: Colors.divider,
              borderTopWidth: 1,
              marginTop: 10,
            }}
          >
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                justifyContent: "space-between",
                paddingHorizontal: 5,
              }}
            >
              <DTText type="bodyRegular16">Total</DTText>
              <Text>1000$</Text>
            </View>
          </View>
          <TouchableWithoutFeedback onPress={bookTour}>
            <View
              style={{
                width: metrics.screenWidth - 24,
                height: 48,
                backgroundColor: "#51cb96",
                borderRadius: 5,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Text
                style={{
                  color: Colors.white,
                  fontWeight: Fonts.fontWeight.medium,
                  fontSize: Fonts.fontSize.xMedium,
                }}
              >
                Book Tour
              </Text>
            </View>
          </TouchableWithoutFeedback>
        </ScrollView>
      </View>
    </Container>
  );
};

export default Cart;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
    // backgroundColor: "green",
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});
