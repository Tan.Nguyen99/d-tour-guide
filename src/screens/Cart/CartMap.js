import React, { useState } from "react";
import { View, Text, StyleSheet } from "react-native";
import MapView, {
  PROVIDER_GOOGLE,
  Marker,
  Callout,
  Polygon,
  Polyline,
} from "react-native-maps";
import { useSelector } from "react-redux";

const CartMap = (props) => {
  return (
    <View style={styles.container}>
      <MapView
        style={styles.map}
        initialRegion={{
          latitude: props.currentLocation.latitude || 37.78825,
          longitude: props.currentLocation.longitude || -122.4324,
          latitudeDelta: props.currentLocation.latitudeDelta || 0.0922,
          longitudeDelta: props.currentLocation.longitudeDelta || 0.0421,
        }}
      >
        <Marker
          coordinate={{
            latitude: props.currentLocation.latitude,
            longitude: props.currentLocation.longitude,
          }}
        ></Marker>
      </MapView>
    </View>
  );
};

export default CartMap;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
    // backgroundColor: "green",
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});
