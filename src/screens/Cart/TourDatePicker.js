import React, { useState, useEffect } from "react";
import {
  View,
  TouchableWithoutFeedback,
  StyleSheet,
  Platform,
  Dimensions,
  Alert,
} from "react-native";
import { Text } from "../../components";
import { Colors, Fonts, Metrics } from "../../themes";
import { NavigationUtils } from "../../navigation";
import moment from "moment";
import { useDispatch } from "react-redux";
import DatePicker from "react-native-date-picker";
import _ from "lodash";

const TourDatePicker = (props) => {
  const [date, setDate] = useState(new Date(moment()));

  const onConfirm = () => {
    const { onReturnDate } = props;
    onReturnDate && onReturnDate(date);
    onDismissOverlay();
  };

  const onDismissOverlay = () => {
    NavigationUtils.dismissOverlay();
  };

  return (
    <View style={styles.container}>
      <View style={styles.datePickerContainer}>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "center",
          }}
        >
          <View
            style={{
              flex: 1,
              flexDirection: "row",
              justifyContent: "flex-end",
              alignItems: "center",
            }}
          >
            <Text>Hour</Text>
          </View>
          <DatePicker
            date={date}
            onDateChange={setDate}
            mode={"time"}
            style={{ flex: 3 }}
            minimumDate={props.startDate}
            androidVariant={"nativeAndroid"}
            locale={"fr"}
            minuteInterval={5}
          />
          <Text style={{ flex: 1, alignSelf: "center" }}>Minute</Text>
        </View>

        <TouchableWithoutFeedback onPress={onConfirm}>
          <View style={styles.buttonContainer}>
            <Text type="regular14" color={Colors.white}>
              Xác nhận
            </Text>
          </View>
        </TouchableWithoutFeedback>
      </View>
    </View>
  );
};

export default TourDatePicker;
const styles = StyleSheet.create({
  container: {
    backgroundColor: "rgba(0,0,0,0.5)",
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
  },
  datePickerContainer: {
    backgroundColor: "white",
    width: Metrics.screenWidth - 24,
    height: Metrics.screenHeight / 2,
    borderRadius: 8,
    justifyContent: "space-around",
  },
  buttonContainer: {
    width: 100,
    height: 50,
    backgroundColor: Colors.primary,
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 8,
  },
});
