import React from "react";
import {
  View,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
} from "react-native";
import PropTypes from "prop-types";
import { Text } from "./index";
import { Metrics, Colors, Fonts } from "../themes";

export default class EmptyView extends React.PureComponent {
  render() {
    const { title, onPress, buttonTitle } = this.props;
    return (
      <View style={styles.container}>
        <Image
          source={require("../assets/img/empty.png")}
          style={{ width: 180, height: 180 }}
        />
        <Text center type="light" sizeType="xMedium">
          {title}
        </Text>
        {buttonTitle && (
          <TouchableWithoutFeedback onPress={onPress}>
            <View
              style={{
                width: Metrics.screenWidth - 120,
                backgroundColor: Colors.primary,
                borderRadius: 5,
                alignItems: "center",
                borderColor: Colors.white,
                borderWidth: 1,
                justifyContent: "center",
                height: 48,
                marginTop: 16,
              }}
            >
              <Text
                style={{
                  color: Colors.white,
                  fontWeight: Fonts.fontWeight.regular,
                  fontSize: Fonts.fontSize.xMedium,
                }}
              >
                {buttonTitle}
              </Text>
            </View>
          </TouchableWithoutFeedback>
        )}
      </View>
    );
  }
}

EmptyView.propTypes = {
  title: PropTypes.string,
};

EmptyView.defaultProps = {};

const styles = StyleSheet.create({
  container: {
    // marginTop: Metrics.screenHeight / 2.5,
    justifyContent: "center",
    flex: 1,
    alignItems: "center",
    marginBottom: 12,
  },
});
